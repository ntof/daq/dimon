
import 'mrest'

export = AppServer
export as namespace AppServer

declare namespace AppServer {
    interface Auth {
        clientID: string,
        clientSecret: string,
        callbackURL: string,
        logoutURL: string
    }
    interface Config {
        port?: number,
        basePath: string,
        noWebSocket?: boolean,
        auth?: Auth,
        dimon: {
            throttle?: number,
            menus: Dimon.Menu[],
            zones: Dimon.Zone[],
            credentials: Dimon.Credentials
        }
    }

    namespace Dimon {
        interface Service {
            name: string,
            available?: boolean,
            type?: string,
            status?: string
        }
        interface BashAction {
            type: string,
            path: string
        }
        interface SSHAction {
            type: string,
            host: string,
            cmd: string
        }
        interface PingAction {
            type: string,
            host: string
        }
        interface SystemctlAction {
            type: string,
            host: string,
            service: string
        }
        interface IPMIResetAction {
            type: string,
            host: string,
            computer: string
        }
        interface IPMIConsoleAction {
            type: string,
            host: string
        }
        interface OCRolloutAction {
            type: string,
            namespace: string,
            deployment: string
        }
        interface PLCAction {
            type: string,
            host: string,
            cmd: string,
            rack?: number,
            slot?: number
        }
        type ActionInfo = BashAction | SSHAction | SystemctlAction |
            IPMIResetAction | IPMIConsoleAction | OCRolloutAction |
            PLCAction | PingAction
        interface Action {
            name: string,
            allowed: string,
            actions: Array<ActionInfo>
        }
        interface Server {
            name: string,
            display: string,
            description: string,
            status?: string,
            messages?: { [key: string]: string },
            services: Array<Service>,
            actions: Array<Action>
        }
        interface Menu {
            name: string,
            actions: Array<Action>
        }
        interface Zone {
            name: string,
            addhExtra: boolean,
            dns: string,
            servers: Array<Server>
        }
        interface Credentials {
            [key: string]: {
                user?: string,
                password?: string,
                path?: string,
                key?: string,
                token?: string
            }
        }
    }
}
