// @ts-check

const
  { Dimon, Status } = require('./Dimon'),
  { ActionRunner, ActAllowed } = require('./Actions/ActionRunner'),
  IPMIConsole = require('./Actions/IPMIConsole'),

  cors = require('cors'),
  bodyParserJson = require('express').json,
  { get, forEach, throttle, has, find, includes, set } = require('lodash');

/**
 * @typedef {import('./types').Dimon.Zone} Zone
 * @typedef {import('./types').Dimon.Menu} Menu
 * @typedef {import('./types').Dimon.Credentials} Credentials
 * @typedef {{ menus: Menu[], zones: Zone[], throttle?: number }} Config
 * @typedef {{ menu?: string, zone?: string, server?: string, name: string }} ActionReq
 *
 * @typedef {import('express').Request} Request
 * @typedef {import('express').IRouter} expressIRouter
 * @typedef {import('express-ws').WithWebsocketMethod & expressIRouter} IRouter
 */

const WS_NORMAL_CLOSURE = 1000;
const WS_GOING_AWAY = 1001;
const WS_INTERNAL_ERROR = 1011;

const ActionRouters = [ IPMIConsole ];

class DimonServer {
  /**
   * @param {Config} config
   */
  constructor(config) {
    /** @type {Menu[]} */ this.menus = get(config, 'menus', []);
    /** @type {Zone[]|null} */ this.zones = null;
    /** @type {Credentials}*/ this.credentials = get(config, 'credentials', {});
    this.dimon = new Dimon(get(config, 'zones'));

    this._id = 0;
    /** @type {{[id: number]: ((zones?: Zone[]) => void)}} */
    this._clients = {};
    this._broadcast = get(config, 'throttle') ?
      throttle(this._broadcast.bind(this), config.throttle) :
      this._broadcast.bind(this);
  }

  /**
   * @param {IRouter} router
   */
  register(router) {
    this.release();

    this.dimon.on('update', this._broadcast);
    this.dimon.monitor();

    if (!router.ws) {
      throw new Error('WebSocket not supported');
    }

    router.ws('/monitoring', (ws) => {
      try {
        /* send menus and current zones */
        ws.send(JSON.stringify({ menus: this.menus }));
        if (this.zones) {
          ws.send(JSON.stringify({ zones: this.zones }));
        }
      }
      catch (err) {
        ws.close(WS_INTERNAL_ERROR, err.message);
      }

      const id = this.subscribe((zones) => {
        if (zones) {
          try {
            ws.send(JSON.stringify({ zones }));
          }
          catch (err) {
            ws.close(WS_INTERNAL_ERROR, err.message);
          }
        }
        else {
          ws.close(WS_GOING_AWAY);
        }
      });

      ws.once('close', () => this.unsubscribe(id));
    });

    /* handling actions */
    router.ws('/action', (ws, req) => {

      ws.on('message', (/** @type {string} */ data) => {
        try {
          const actReq = JSON.parse(data);
          const action = this._getAction(actReq);
          if (!action) { throw new Error('Action not found'); }

          const server = this.getServer(actReq.zone, actReq.server);
          this._checkPermission(action, get(req.user, [ 'cern_roles' ]),
            get(server, 'status'));

          const ar = new ActionRunner(this.credentials);
          ar.on('stdout', (/** @type {string} */data) => {
            ws.send(JSON.stringify({ type: 'stdout', data }));
          });
          ar.on('stderr', (/** @type {string} */data) => {
            ws.send(JSON.stringify({ type: 'stderr', data }));
          });
          ar.on('error', (/** @type {Error} */err) => {
            ws.send(JSON.stringify({ type: 'error', data: err.message }));
          });
          ar.on('return', (/** @type {any} */data) => {
            ws.send(JSON.stringify({ type: 'return', data }));
          });

          ar.run(action)
          .catch((/** @type {Error} */err) => {
            ws.send(JSON.stringify({ type: 'error', data: err.message }));
          })
          .finally(() => {
            ar.removeAllListeners();
            ws.close(WS_NORMAL_CLOSURE, 'Action completed');
          });

          ws.once('close', () => ar.removeAllListeners());
        }
        catch (err) {
          ws.close(WS_INTERNAL_ERROR, err.message);
        }
      });

      ws.once('close', () => ws.removeAllListeners());
    });

    router.use('/action', bodyParserJson(), (req, res, next) => {
      try {
        const action = this._getAction(
          /** @type {ActionReq} */(req.query || req.body));
        if (!action) { throw new Error('Action not found'); }

        this._checkPermission(action, get(req.user, [ 'cern_roles' ]));
        set(req, [ 'locals', 'credentials' ], this.credentials);
        return next();
      }
      catch (err) {
        return next(err);
      }
    }, ...ActionRouters);

    router.get('/ping', cors(), async (req, res) => {
      var ret;
      const action = { actions: [ { type: 'ping', host: req.query.host } ] };

      const ar = new ActionRunner(this.credentials);
      ar.once('return', (data) => { ret = data; });

      try {
        await ar.run(/** @type {AppServer.Dimon.Action} */(action));
        res.status(200).json({ reachable: (ret === 0) ? true : false });
      }
      catch (err) {
        ar.removeAllListeners();
        res.status(500).send(err.message);
      }
    });
  }

  /**
   * @param {ActionReq} actionRequest
   * @return {AppServer.Dimon.Action|undefined}
   */
  _getAction(actionRequest) {
    let act;

    if (has(actionRequest, 'menu')) {
      const menu = find(this.menus, [ 'name', actionRequest.menu ]);
      act = find(get(menu, 'actions', []), [ 'name', actionRequest.name ]);
    }
    else if (has(actionRequest, 'zone') && has(actionRequest, 'server')) {
      // @ts-ignore: 'zone' and 'server' just checked
      const server = this.getServer(actionRequest.zone, actionRequest.server);
      act = find(get(server, 'actions', []), [ 'name', actionRequest.name ]);
    }
    return act;
  }

  /**
   * @param {AppServer.Dimon.Action} action
   * @param {string[]} userRoles
   * @param {string} [serverStatus]
   */
  _checkPermission(action, userRoles, serverStatus) { // eslint-disable-line complexity
    const isExpert = includes(userRoles, 'expert');
    if (action.allowed === ActAllowed.EXPERT && !isExpert) {
      throw new Error('Action requires the expert role');
    }
    else if (action.allowed === ActAllowed.NEVER) {
      throw new Error('Action not allowed');
    }
    else if (isExpert) {
      // experts can do (almost) anything
      return;
    }
    else if (action.allowed === ActAllowed.ONERROR &&
      serverStatus !== Status.ERROR) {
      throw new Error('Action allowed on server error status');
    }
    else if (action.allowed === ActAllowed.ONWARNING &&
      serverStatus !== Status.WARNING && serverStatus !== Status.ERROR) {
      throw new Error('Action allowed on server warning status');
    }
  }

  /**
   * @param {string} zone
   * @param {string} server
   * @return {AppServer.Dimon.Server|undefined}
   */
  getServer(zone, server) {
    const zn = find(this.zones, [ 'name', zone ]);
    return find(get(zn, 'servers', []), [ 'name', server ]);
  }

  /**
   * @param {Zone[]} zones
   */
  _broadcast(zones) {
    this.zones = zones;
    forEach(this._clients, (cb) => cb(zones));
  }

  /**
   * @param {(zones?: Zone[]) => void} cb
   * @return {number}
   */
  subscribe(cb) {
    const id = this._id++;
    this._clients[id] = cb;
    return id;
  }

  /**
   * @param {number} id
   */
  unsubscribe(id) {
    delete this._clients[id];
  }

  release() {
    forEach(this._clients, (cb) => cb());
    this.dimon.removeListener('update', this._broadcast);
    this.dimon.release();
    this._clients = {};
    this._id = 0;
    this.zones = null;
  }
}

module.exports = DimonServer;
