// @ts-check
console.warn('Loading stub configuration');

module.exports = {
  title: 'DIMon',
  basePath: '',
  beta: true,
  auth: {
    clientID: "base-website-template",
    clientSecret: "fce99307-a575-4fad-bff3-57214e569351",
    callbackURL: "http://localhost:8080/auth/callback",
    logoutURL: "http://localhost:8080"
  },
  dimon: {
    throttle: 2000,
    menus: [
      {
        name: "Monitoring",
        actions: [
          { name: "Reload pages", allowed: "always", actions: [
            { type: "exec", path: "~/DIMon/reloadDisplay.sh"  }
          ] },
          { name: "Reboot fixed display PC", allowed: "expert", actions: [
            { type: "exec", path: "~/DIMon/rebootDisplay.sh" }
          ] },
          { name: "Restart DIM to web",  allowed: "expert", actions: [
            { type: "systemctl", host: "ntofdaq-pr01",
              service: "nTOF_DIMToWeb" }
          ] },
          { name: "Action test", allowed: 'always', actions: [
            { type: "ssh", user: "root", host: "localhost", cmd: "" }
          ] }
        ]
      }
    ],
    zones: [
      { name: 'EAR1', addhExtra: true, dns: 'dns-ear1.cern.ch', servers: [
        { name: 'DIS_DNS_1', display: 'DIM DNS 1',
          description: 'DIM Name Server 1',
          services: [
            { name: "DIS_DNS_1/service_1" },
            { name: "DIS_DNS_1/service_2" }
          ],
          actions: [
            { name: 'Critical action test', allowed: 'onError', actions: [
              { type: "systemctl", host: "localhost", service: "" }
            ] }
          ]
        },
        { name: 'DIS_DNS_2', display: 'DIM DNS 2',
          description: 'DIM Name Server 2',
          services: [ { name: "DIS_DNS_2/service" } ],
          actions: [] },
        { name: 'DIS_DNS_3', display: 'DIM DNS 3',
          description: 'DIM Name Server 3',
          services: [ { name: "DIS_DNS_3/CLIENT_LIST" } ],
          actions: [
            { name: 'OnError action', allowed: 'onError', actions: [] },
            { name: 'OnWarning action', allowed: 'onWarning', actions: [] },
            { name: 'Expert action', allowed: 'expert', actions: [] },
            { name: 'Always allowed action', allowed: 'always', actions: [] },
            { name: 'Never allowed action', allowed: 'never', actions: [] }
          ] }
      ] },
      { name: 'EAR2', addhExtra: true, dns: 'dns-ear2.cern.ch', servers: [
        { name: 'DIS_DNS_1', display: 'DIM DNS 1',
          description: 'DIM Name Server 1',
          services: [ { name: 'DIS_DNS_1/CLIENT_LIST'  } ],
          actions: [
            { name: 'Bash Script test', allowed: 'always', actions: [
              { type: 'exec', path: '~/TEST.sh' }
            ] },
            { name: 'IPMI Console test', allowed: 'always', actions: [
              { type: 'ipmi-console', host: 'localhost' }
            ] },
            { name: 'IPMI Reset test', allowed: 'always', actions: [
              { type: 'ipmi-reset', host: 'localhost' }
            ] },
            { name: 'Ping test', allowed: 'always', actions: [
              { type: 'ping', host: 'localhost' }
            ] },
            { name: 'SSH Cmd test', allowed: 'always', actions: [
              { type: 'ssh', user: 'root', host: 'localhost', cmd: 'echo TEST' }
            ] },
            { name: 'Systemctl test', allowed: 'always', actions: [
              { type: 'systemctl', host: 'localhost', service: 'TEST' }
            ] },
            { name: 'OC Rollout test', allowed: 'always', actions: [
              { type: 'oc-rollout', namespace: 'ns', deployment: 'dp' }
            ] },
            { name: 'PLC test', allowed: 'always', actions: [
              { type: 'plc', host: 'PLCHost', cmd: 'DB1,W4.1',
                rack: 0, slot: 2 }
            ] }
          ] }
      ] }
    ],
    credentials: {
      "ssh://localhost": { user: 'root' }
    }
  }
};
