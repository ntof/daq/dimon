// @ts-check

const
  {
    get, forEach, has, isEmpty, toNumber, every, assign, pick
  } = require('lodash'),
  { EventEmitter } = require('events'),

  { timeout } = require('@cern/prom'),
  { DnsClient, DicDis, DisClient } = require('@cern/dim');

/**
 * @typedef {Array<AppServer.Dimon.Zone>} Config
 *
 * @typedef {{
 *  connected: boolean,
 *  dnsClient: dim.DnsClient|null,
 *  dimClients: Array<dim.DicDis.DicServicesList>
 * }} DimInfo
 */

const Status = {
  OK: 'OK',
  WARNING: 'WARNING',
  ERROR: 'ERROR'
};

const ErrMsg = {
  NO_SERVICE_LIST: 'Cannot check services',
  DNS_DISCONNECTED: 'DNS Disconnected',
  DNS_UNREACHABLE: 'Failed to connect to DNS',
  SERVER_DISCONNECTED: 'Server Disconnected'
};

/**
 * A client for monitoring the status of DIM services
 * @extends EventEmitter
 * @emits :
 *  - 'update' when zones are updated
 *  - 'released' when this service has been released
 */

class Dimon extends EventEmitter {
  /**
   * @param {Config} config
   */
  constructor(config) {
    super();
    this.zones = config;
    /** @type {{ [zone: string]: DimInfo }} */
    this._zoneMap = {};
  }

  monitor() {
    this.release();

    /* monitor DIM Servers/Services and update zones */
    forEach(this.zones, (zone) => {
      if (isEmpty(zone)) { return; }
      if (!get(this._zoneMap[zone.name], 'dnsClient')) {
        const dns = zone.dns.split(':'); // host:port
        const dnsClient = new DnsClient(dns[0], toNumber(dns[1]) || undefined);

        /** @type {DimInfo} */
        const dimInfo = this._zoneMap[zone.name] =
          { connected: false, dnsClient, dimClients: [] };

        forEach(zone.servers, (server) => {
          server.messages = {};
          const cli = new DicDis.DicServicesList(server.name, dnsClient);

          cli.on('value', this.update.bind(this, zone.name, server));
          cli.once('released', () => cli.removeListener('value',
            this.update.bind(this, zone.name, server)));

          /* first services check */
          timeout(cli.promise(), DisClient.TIMEOUT).catch(() => {
            server.status = Status.ERROR;
            assign(server.messages, pick(ErrMsg, 'NO_SERVICE_LIST'));
            this.emit('update', this.zones);
          });

          dimInfo.dimClients.push(cli);
        });

        const onConnect = () => {
          dnsClient.once('disconnect', onDisconnect);
          dimInfo.connected = true;

          forEach(zone.servers, (server) => {
            /* server status */
            if (every(server.services, 'available')) {
              server.status = Status.OK;
            }
            else if (has(server.messages, 'SERVER_DISCONNECTED') ||
              has(server.messages, 'NO_SERVICE_LIST')) {
              server.status = Status.ERROR;
            }
            else {
              server.status = Status.WARNING;
            }
            if (server.messages) {
              delete server.messages.DNS_DISCONNECTED;
              delete server.messages.DNS_UNREACHABLE;
            }
          });
          this.emit('update', this.zones);
        };

        const onDisconnect = () => {
          dnsClient.once('connect', onConnect);
          dimInfo.connected = false;

          forEach(zone.servers, (server) => {
            server.status = Status.ERROR;
            assign(server.messages, pick(ErrMsg, 'DNS_DISCONNECTED'));
          });
          this.emit('update', this.zones);
        };
        dnsClient.once('connect', onConnect);

        dnsClient.connect().catch(() => {
          forEach(zone.servers, (server) => {
            server.status = Status.ERROR;
            assign(server.messages, pick(ErrMsg, 'DNS_UNREACHABLE'));
          });
          this.emit('update', this.zones);
        });
      }
    });
  }

  /**
   * @param {string} zone
   * @param {AppServer.Dimon.Server} server
   * @param {{[service: string]: any}|undefined} services
  */
  update(zone, server, services) {
    if (services === undefined) { /* server disconnected */
      server.status = Status.ERROR;
      assign(server.messages, pick(ErrMsg, 'SERVER_DISCONNECTED'));

      forEach(server.services, (service) => {
        service.available = false;
      });
    }
    else { /* services check */
      server.status = Status.OK;
      forEach(server.services, (service) => {
        service.available = has(services, service.name);
        if (!service.available) { server.status = Status.WARNING; }
      });

      if (!this._zoneMap[zone].connected) {
        // zone DNS not connected
        server.status = Status.ERROR;
      }

      if (server.messages) {
        delete server.messages.NO_SERVICE_LIST;
        delete server.messages.SERVER_DISCONNECTED;
      }
    }
    this.emit('update', this.zones);
  }

  /**
   * @param {DimInfo} zone
   */
  _releaseDimClients(zone) {
    if (zone) {
      forEach(zone.dimClients, (cli) => cli.release());
      zone.dimClients = [];

      if (zone.dnsClient) {
        zone.dnsClient.removeAllListeners('connect');
        zone.dnsClient.removeAllListeners('disconnect');
        zone.dnsClient.unref();
        zone.dnsClient = null;
      }
    }
  }

  release() {
    forEach(this._zoneMap, (zone) => this._releaseDimClients(zone));
    this.emit('released');
    this._zoneMap = {};
  }
}

module.exports = { Dimon, Status, ErrMsg };
