
const { merge, defaultTo, toString } = require('lodash');
const yaml = require('js-yaml');
const fs = require('fs');
const path = require('path');
const debug = require('debug')('yaml:loadAll');

/**
 * @param  {string} dir
 * @param  {RegExp} [pattern=/^config.*.yml$/] [description]
 * @return {AppServer.Config|null}
 */
function loadAll(dir, pattern = /^config.*\.yml$/) {
  try {
    /** @type {string[]} */
    const files = [];
    fs.readdirSync(dir).forEach((f) => {
      if ((f.endsWith('.json') || f.endsWith('.yml')) &&
          (!pattern || pattern.test(f))) {
        files.push(f);
      }
    });
    debug('Loading files: %o from "%s"', files, dir);
    return files.reduce((ret, f) => merge(ret,
      yaml.load(toString(fs.readFileSync(path.join(dir, f))))), null);
  }
  catch (e) {
    debug(e.message);
    return null;
  }
}

/** @type {AppServer.Config} */
const config = loadAll('/etc/app') || loadAll('./') ||
  require('./config-stub'); /* eslint-disable-line global-require */

config.port = defaultTo(config.port, 8080);
config.basePath = defaultTo(config.basePath, '');

module.exports = /** @type {AppServer.Config} */ (config);
