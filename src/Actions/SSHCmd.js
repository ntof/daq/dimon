// @ts-check

const
  { writeFileSync } = require('fs'),
  { get, includes } = require('lodash'),
  tmp = require('tmp'),

  Action = require('./Action');

/**
 * @typedef {import('tmp').FileResult} FileResult
 * @typedef {AppServer.Dimon.SSHAction} SSHAction
 */

/**
 * @extends Action
 */
class SSHCmd extends Action {

  /**
   * @param {AppServer.Dimon.ActionInfo} info
   * @param {AppServer.Dimon.Credentials} credentials
   */
  constructor(info, credentials) {
    /** @type {FileResult|null} */
    let tmpFile = null;

    const host = get(info, 'host', '');
    const defUser = get(credentials, [ `ssh://${host}`, 'user' ], 'root');
    const user = get(info, 'user', defUser);

    let keyPath = get(credentials, [ `ssh://${host}`, 'path' ]);
    if (!keyPath) {
      tmpFile = tmp.fileSync();
      writeFileSync(tmpFile.name,
        get(credentials, [ `ssh://${host}`, 'key' ], '') + '\n');
      keyPath = tmpFile.name;
    }

    super();
    if (/** @type {SSHAction} */(info).cmd) {
      this.cmd = 'ssh';
      this.sshCmd = /** @type {SSHAction} */(info).cmd;
      this.args = [ '-F', 'none', '-T',
        '-o', 'StrictHostKeyChecking=no',
        '-o', 'UserKnownHostsFile=/dev/null',
        '-o', 'LogLevel=QUIET',
        '-i', `${keyPath}`, `${user}@${host}`, this.sshCmd ];
      this.opts = { shell: false };
      this.return = 'Command performed.';
    }

    if (tmpFile) {
      this.once('close', tmpFile.removeCallback.bind(tmpFile));
    }
  }

  /**
   * @param {number|null} code
   * @param {NodeJS.Signals|null} signal
   */
  close(code, signal) {
    // React correctly to reboot since code is in error because of lost connection
    if (includes(this.args, '/sbin/reboot') && code === 255) {
      code = 0;
    }
    super.close(code, signal);
  }
}

/**
 * @extends SSHCmd
 */
class Systemctl extends SSHCmd {
  /**
   * @param {AppServer.Dimon.ActionInfo} info
   * @param {AppServer.Dimon.Credentials} credentials
   */
  constructor(info, credentials) {
    const service = get(info, 'service');
    /** @type {SSHAction} */(info).cmd = service ?
      `systemctl restart ${service} &&\
      echo "Waiting for '${service}' to restart..." && sleep 5 &&\
      { systemctl status ${service} || true; }` : '';
    super(/** @type {SSHAction} */(info), credentials);
    this.service = service;
    this.return = 'Service restarted.';
  }

  async exec() {
    if (this.service) { this.stdout(`Restarting '${this.service}'...`); }
    return super.exec();
  }
}

module.exports = { SSHCmd, Systemctl };
