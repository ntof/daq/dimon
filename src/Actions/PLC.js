// @ts-check

const
  { cond, get, forOwn, conforms, isString,
    isNil, negate, isEmpty, isArray } = require('lodash'),
  NodeS7 = require('nodes7'),
  { makeDeferred } = require('@cern/prom'),

  Action = require('./Action');

/**
 * @typedef {{ address: string, value?: NodeS7.S7Types }} PlcCmd
 */

/**
 * @extends Action
 */
class PLC extends Action {

  /**
   * @param {AppServer.Dimon.ActionInfo} info
   */
  constructor(info) {
    super();
    this.host = get(info, 'host');
    this.rack = get(info, 'rack', 0);
    this.slot = get(info, 'slot', 2);
    this.cmd = get(info, 'cmd', '');

    this.cli = new NodeS7({ silent: true });

    this.run = cond([
      /** @type {[function(PlcCmd): boolean, function(PlcCmd): Promise<string>]} */
      ([ conforms({ 'address': isString, 'value': negate(isNil) }),
        this.write ]),
      [ conforms({ 'address': isString }), this.read ],
      [ () => true,
        (cmd) => {
          throw new Error(`Wrong PLC command [${JSON.stringify(cmd)}].`);
        } ]
    ]);
  }

  async exec() {
    if (!this.host) { throw new Error('Host missing'); }
    if (isEmpty(this.cmd)) { throw new Error('Command(s) missing'); }
    if (!isNil(this._retProm)) { throw new Error('Execution in progress'); }

    const opts = { host: this.host, rack: this.rack, slot: this.slot };

    /** @type {Array<PlcCmd>} */
    let plcCmds = [];
    if (isString(this.cmd)) {
      plcCmds.push({ address: this.cmd });
    }
    else if (!isArray(this.cmd)) {
      plcCmds = [ this.cmd ];
    }

    this.stdout(`Connecting to PLC... (host: '${this.host}'\n`);
    this._retProm = makeDeferred();

    this.cli.initiateConnection(opts, async (err) => {
      if (err) {
        // @ts-ignore: '_retProm' cannot be null here
        this._retProm.reject(new Error(
          `Failed to connect to '${this.host}': ${err.message}.`));
        this._retProm = null;
        return;
      }
      this.stdout(`Connected.\n`);

      for (const cmd of plcCmds) {
        await this.run(cmd)
        .then(this.stdout)
        .catch(this.stderr);
      }

      this.cli.dropConnection(() => this.stdout('Disconnected.\n'));

      // @ts-ignore: '_retProm' cannot be null here
      this._retProm.resolve('PLC command(s) performed.');
      this._retProm = null;
    });
    return this._retProm.promise;
  }

  /**
   * @param {PlcCmd} cmd
   * @return {Promise<string>}
   */
  async write(cmd) {
    if (!this._retProm) {
      throw new Error(`Cannot execute command '${JSON.stringify(cmd)}'`);
    }
    const done = makeDeferred();

    const code = this.cli.writeItems(cmd.address,
      /** @type {NodeS7.S7Types} */(cmd.value), (err) => {
        if (err) {
          done.reject(
            new Error(`Failed to perform command '${JSON.stringify(cmd)}': ` +
            'invalid command.'));
        }
        else {
          done.resolve(`Command '${JSON.stringify(cmd)}' done.`);
        }
      });

    if (code !== 0) {
      done.reject(
        new Error(`Failed to perform command '${JSON.stringify(cmd)}': ` +
        'another writing is in progress.'));
    }
    return done.promise;
  }

  /**
   * @param {PlcCmd} cmd
   * @return {Promise<string>}
   */
  async read(cmd) {
    if (!this._retProm) {
      throw new Error(`Cannot execute command '${JSON.stringify(cmd)}'`);
    }
    const done = makeDeferred();

    this.cli.addItems(cmd.address);
    this.cli.readAllItems((err, values) => {
      if (err) {
        done.reject(
          new Error(`Failed to perform command '${JSON.stringify(cmd)}': ` +
          'invalid command.\n'));
      }
      else if (values) {
        let output = 'Values read:\n';
        forOwn(values, (val, key) => { output += `- '${key}': ${val}\n`; });
        done.resolve(output);
      }
      else {
        done.reject(new Error(
          `Command '${JSON.stringify(cmd)}' performed but no value read.\n`));
      }
    });
    return done.promise;
  }
}

module.exports = PLC;
