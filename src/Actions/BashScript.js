// @ts-check

const
  Action = require('./Action');

/**
 * @typedef {AppServer.Dimon.BashAction} BashAction
 */

class BashScript extends Action {

  /**
   * @param {AppServer.Dimon.ActionInfo} info
   */
  constructor(info) {
    super();
    if (/** @type {BashAction} */(info).path) {
      this.cmd = 'sh';
      this.args = [ /** @type {BashAction} */(info).path ];
      this.opts = { shell: true };
    }
  }
}

module.exports = BashScript;
