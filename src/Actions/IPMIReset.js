// @ts-check

const
  { get } = require('lodash'),

  Action = require('./Action');

class IPMIReset extends Action {

  /**
   * @param {AppServer.Dimon.ActionInfo} info
   * @param {AppServer.Dimon.Credentials} credentials
   */
  constructor(info, credentials) {
    const host = get(info, 'host', 'undefined');
    const user = get(credentials, [ `ipmi://${host}`, 'user' ], 'admin');
    const pass = get(credentials, [ `ipmi://${host}`, 'password' ], 'password');

    super();
    this.cmd = 'ipmipower';
    this.args = [
      '-h', host,
      '-u', user,
      '-p', pass,
      '--driver-type=LAN_2_0', // IPMI 2.0
      '--privilege-level=ADMIN',
      '--reset', // hard-reset power cmd
      '--on-if-off' // force a power on cmd if remote machine's power is off
    ];
    this.opts = { shell: false };
    this.return = 'Reset done! Waiting for operating system to be up...';
  }
}

module.exports = IPMIReset;
