// @ts-check

const
  EventEmitter = require('events'),
  { get, isEmpty, isNil } = require('lodash'),

  Ping = require('./Ping'),
  BashScript = require('./BashScript'),
  { SSHCmd, Systemctl } = require('./SSHCmd'),
  OCRollout = require('./OCRollout'),
  IPMIReset = require('./IPMIReset'),
  PLC = require('./PLC');

const ActType = {
  PING: 'ping',
  IPMI_CONSOLE: 'ipmi-console',
  IPMI_RESET: 'ipmi-reset',
  SSH: 'ssh',
  SYSTEMCTL: 'systemctl', // restart
  PLC: 'plc',
  EXEC: 'exec',
  OC_ROLLOUT: 'oc-rollout'
};

const ActAllowed = {
  ALWAYS: 'always',
  NEVER: 'never',
  ONERROR: 'onError',
  ONWARNING: 'onWarning',
  EXPERT: 'expert'
};

/**
 * @typedef {AppServer.Dimon.ActionInfo} ActionInfo
 * @typedef {AppServer.Dimon.Credentials} Credentials
 * @typedef {import('./Action')} Action
 */

/** @type {{ [key: string]:
 * new (info: ActionInfo, credentials: Credentials) => Action}} */
const ActionFactory = {
  [ActType.PING]: Ping,
  [ActType.IPMI_RESET]: IPMIReset,
  [ActType.EXEC]: BashScript,
  [ActType.SSH]: SSHCmd,
  [ActType.SYSTEMCTL]: Systemctl,
  [ActType.PLC]: PLC,
  [ActType.OC_ROLLOUT]: OCRollout
};

/**
 * @extends EventEmitter
 * @emits :
 *  - 'stdout' on a new data chunk on a sub-action's stdout
 *  - 'stderr' on a new data chunk on a sub-action's stderr
 *  - 'error' on an error of a sub-action
 *  - 'return' on a return value of a sub-action
 */
class ActionRunner extends EventEmitter {

  /**
   * @param {Credentials} credentials
   */
  constructor(credentials) {
    super();
    this.credentials = credentials;
    /** @type {Action|null} */
    this._proc = null;
  }

  /**
   * @param {AppServer.Dimon.Action} action
   * @return {Promise<boolean>}
   */
  async run(action) {
    const subActions = get(action, 'actions', []);
    if (isEmpty(subActions)) { throw new Error('No action defined'); }

    for (const act of subActions) {
      const Action = ActionFactory[act.type];
      if (!Action) { throw new Error(`No such action [${act.type}]\n`); }

      this._proc = new Action(act, this.credentials);
      this._proc.on('stdout', this.emit.bind(this, 'stdout'));
      this._proc.on('stderr', this.emit.bind(this, 'stderr'));
      this._proc.on('error', this.emit.bind(this, 'error'));

      await this._proc.exec()
      .then((data) => {
        if (!isNil(data)) { this.emit('return', data);}
      })
      .finally(this._proc.removeAllListeners.bind(this._proc));
    }
    return true;
  }
}

module.exports = { ActionRunner, ActType, ActAllowed };
