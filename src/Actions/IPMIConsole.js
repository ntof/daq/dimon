// @ts-check

const
  IPMIConsole = require('express').Router(),
  { get, isEmpty, toString } = require('lodash'),
  superagent = require('superagent'),
  { DOMParser, XMLSerializer } = require('xmldom');


IPMIConsole.get('/ipmi-console/:host', async (req, res, next) => {
  const credentials = get(req, [ 'locals', 'credentials' ]);
  const host = req.params.host;

  try {
    // fetch session credentials
    const loginUsr = get(credentials, [ `ipmi://${host}`, 'user' ]);
    if (!loginUsr) {
      throw new Error(`'${host}' login error: user missing.`);
    }

    const loginPsw = get(credentials, [ `ipmi://${host}`, 'password' ]);
    if (!loginPsw) {
      throw new Error(`'${host}' login error: password missing.`);
    }

    const agent = superagent.agent(); // to save cookies

    // login
    await agent.post(`https://${host}/cgi/login.cgi`)
    .disableTLSCerts() /* FIXME: update certs and remove this line */
    .send(`name=${loginUsr}&pwd=${loginPsw}`);

    // check Session ID
    const SID = agent.jar.getCookie('SID',
      { path: '/', domain: host, secure: true, script: false });

    if (SID && isEmpty(SID.value)) {
      throw new Error(`'${host}' login error: cannot open a session. ` +
        `Please check credentials.`);
    }

    // generate the 'iKVM.jnlp' file
    const iKVM = await agent
    .get(`https://${host}/cgi/url_redirect.cgi?url_name=ikvm&url_type=jwsk`)
    .disableTLSCerts() /* FIXME: update certs and remove this line */
    .set('Referer', `http://${host}/cgi/url_redirect.cgi?url_name=man_ikvm`)
    .then((/** @type {any} */res) => {

      // Fix corrupted jnlp on the fly 'codebase' attribute and 'host' argument
      const doc = new DOMParser().parseFromString(toString(res.body));
      doc.documentElement.setAttribute('codebase', `https://${host}/`);
      doc.getElementsByTagName('argument')[0].textContent = host;

      return (new XMLSerializer()).serializeToString(doc);
    });

    return res
    .set('Content-Disposition', `attachment; filename="iKVM.jnlp"`)
    .set('Content-Type', 'application/x-java-jnlp-file')
    .status(200)
    .send(iKVM);
  }
  catch (err) {
    return next(err);
  }
});

module.exports = IPMIConsole;

