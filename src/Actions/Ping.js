// @ts-check

const
  Action = require('./Action');

/**
 * @typedef {AppServer.Dimon.PingAction} PingAction
 */

class Ping extends Action {

  /**
   * @param {AppServer.Dimon.ActionInfo} info
   */
  constructor(info) {
    super();
    if (/** @type {PingAction} */(info).host) {
      this.cmd = 'ping';
      this.args = [ '-qc', '1', /** @type {PingAction} */(info).host ];
      this.opts = { shell: false };
      this.return = 0; // exit code
    }
  }
}

module.exports = Ping;
