// @ts-check
const
  EventEmitter = require('events'),
  { bindAll, toString } = require('lodash'),
  { spawn } = require('child_process'),
  { makeDeferred } = require('@cern/prom');

/**
 * @typedef {import('child_process').ChildProcessWithoutNullStreams} ChildProcess
 */

/**
 * @extends EventEmitter
 * @emits :
 *  - 'stdout' whenever there is a new data chunk on the child process's stout
 *  - 'stderr' whenever there is a new data chunk on the child process's stderr
 *  - 'error' when an error occurs in the child process
 *  - 'close' when the action has ended
 */
class Action extends EventEmitter {

  constructor() {
    super();
    /** @type {string|null} */
    this.cmd = null;
    /** @type {string[]} */
    this.args = [];
    this.opts = {};
    /** @type {ChildProcess|null} */
    this._proc = null;
    /** @type {prom.Deferred<any>|null} */
    this._retProm = null;
    /** @type {any} */
    this.return = undefined;

    bindAll(this, [ 'stdout', 'stderr', 'error', 'close' ]);
  }

  /**
   * @return {Promise<any>}
   */
  async exec() {
    if (!this._proc) {
      if (!this.cmd) { throw new Error('Command missing'); }
      this._retProm = makeDeferred();
      this._proc = spawn(this.cmd, this.args, this.opts);
      this._proc.stdout.on('data', this.stdout);
      this._proc.stderr.on('data', this.stderr);
      this._proc.on('error', this.error);
      this._proc.once('close', this.close);
    }

    if (!this._retProm) { throw new Error('Failed to execute action'); }
    return this._retProm.promise;
  }

  /**
   * @param {Buffer|string} chunk
   */
  stdout(chunk) { this.emit('stdout', toString(chunk)); }

  /**
   * @param {Buffer|string} chunk
   */
  stderr(chunk) { this.emit('stderr', toString(chunk)); }

  /**
   * @param {Error} err
   */
  error(err) { this.emit('error', err); }


  /**
   * @param {number|null} code
   * @param {NodeJS.Signals|null} signal
   */
  close(code, signal) {
    if (this._proc && this._retProm) {
      this._proc.stdout.removeListener('data', this.stdout);
      this._proc.stderr.removeListener('data', this.stderr);
      this._proc.stderr.removeListener('error', this.error);

      if (code !== 0) {
        this._retProm.reject(new Error(code ?
          `Exited with code: ${code}` :
          `Terminated due to signal: ${signal}`));
      }
      else {
        this._retProm.resolve(this.return);
      }

      this._retProm = null;
      this._proc = null;
    }
    this.emit('close');
  }
}

module.exports = Action;
