// @ts-check

const
  { get, set, clone } = require('lodash'),
  superagent = require('superagent'),

  Action = require('./Action');

/**
 * @param  {any} config
 * @param  {string[]} path
 * @param  {any} value
 * @return {any}
 */
function patchPrepare(config, path, value) {
  const patchPath = [];
  const objPath = clone(path);

  for (let obj = config; (objPath.length !== 0) && (obj !== undefined);) {
    obj = obj[objPath[0]];
    patchPath.push(objPath[0]);
    objPath.shift();
  }
  return [ {
    op: 'add', path: '/' + patchPath.join('/'),
    value: objPath.length ? set({}, objPath, value) : value
  } ];
}

/**
 * @extends Action
 */
class OCRollout extends Action {

  /**
   * @param {AppServer.Dimon.ActionInfo} info
   * @param {AppServer.Dimon.Credentials} credentials
   */
  constructor(info, credentials) {
    super();
    const host = get(info, 'host', '');
    this.ns = get(info, 'namespace');
    this.dplm = get(info, 'deployment');

    // API endpoint + auth token
    this.url = `https://${host}/apis/apps/v1/namespaces/${this.ns}` +
      `/deployments/${this.dplm}`;
    this.token = get(credentials, [ this.url, 'token' ]);
  }

  async exec() {
    if (!this.token) {
      throw new Error(
        `Failed to restart '${this.ns}/${this.dplm}': token missing`);
    }
    try {
      const reply = await superagent.get(this.url)
      .set('Authorization', `Bearer ${this.token}`)
      .set('Accept', 'application/json');

      const patch = patchPrepare(reply.body,
        [ 'spec', 'template', 'metadata', 'annotations' ],
        { "kubectl.kubernetes.io/restartedAt": new Date().toISOString() });

      await superagent.patch(this.url)
      .set('Authorization', `Bearer ${this.token}`)
      .set('Content-Type', 'application/json-patch+json')
      .set('Accept', 'application/json')
      .send(JSON.stringify(patch));
      return `'${this.ns}/${this.dplm}' restarted.`;
    }
    catch (err) {
      throw new Error(
        `Failed to restart '${this.ns}/${this.dplm}': ${err.message}`);
    }
  }
}

module.exports = OCRollout;
