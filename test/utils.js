const
  { makeDeferred, timeout } = require('@cern/prom'),
  { defaultTo, toString, isEqual, hasIn, invoke } = require('lodash'),
  debug = require('debug')('test');


function waitFor(obj, evt, test, ms) {
  const def = makeDeferred();
  const stack = (new Error()).stack;

  const cb = function(...args) {
    debug('waitFor: event received: %s', evt);
    const ret = test(...args);
    if (ret) {
      def.resolve(ret);
    }
  };

  // obj can be an EventEmitter or an EventSource object
  const prop = hasIn(obj, 'addEventListener') ?
    'addEventListener' : 'on';
  invoke(obj, prop, evt, cb);

  return timeout(def.promise, defaultTo(ms, 1000))
  .catch((err) => {
    err.stack = stack;
    throw err;
  })
  .finally(function() {
    if (def.isPending) {
      def.reject();
    }
    const prop = hasIn(obj, 'removeEventListener') ?
      'removeEventListener' : 'removeListener';
    invoke(obj, prop, evt, cb);
  });
}


function waitForValue(obj, evt, test, value, ms) {
  let _val = null;

  return waitFor(obj, evt, (...args) => {
    _val = test(...args);
    return isEqual(_val, value);
  }, ms)
  .catch((err) => {
    const msg = "Invalid result value: " + toString(_val) + " != " + value;
    if (err instanceof Error) {
      err.message = msg;
      throw err;
    }
    throw new Error(msg);
  });
}

module.exports = { waitFor, waitForValue };
