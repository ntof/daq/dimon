'use strict';

const
  { expect } = require('chai'),
  { afterEach, beforeEach, describe, it } = require('mocha'),

  http = require('http'),

  AppServer = require('../src');

describe('WebApp Server', function() {
  let env = {};

  beforeEach(async function() {
    await AppServer.listen();
    env.addr = AppServer.address();
  });

  afterEach(function() {
    AppServer.close();
    env = {};
  });

  it(`serves authenticated requests`, (done) => {
    http.get(`http://localhost:${env.addr.port}/`,
      (res) => {
        // Redirecting to authentication
        expect(res.statusCode).to.be.equal(302);
        expect(res.headers.location).to.be.equal('/auth');
        done();
      });
  });

  it(`serves resources in '/dist' without any authentication`, (done) => {
    let buff = '';
    http.get(`http://localhost:${env.addr.port}/dist/img/favicon.svg`,
      (res) => {
        expect(res.statusCode).to.be.equal(200);

        res
        .on('error', done)
        .on('data', (chunk) => { buff += chunk; })
        .on('end', () => {
          expect(buff).to.contain('</svg>');
          done();
        });
      });
  });
});
