// @ts-check

const
  { expect } = require('chai'),
  { afterEach, beforeEach, describe, it } = require('mocha'),
  { replace, restore } = require('sinon'),
  { invoke, get, cloneDeep, stubTrue } = require('lodash'),
  tmp = require('tmp'),
  { writeFileSync } = require('fs'),
  WebSocket = require('ws'),
  http = require('http'),

  { DisNode, DnsClient, DnsServer, NodeInfo, DisClient } = require('@cern/dim'),
  { waitForValue, waitFor } = require('./utils'),
  { Status, ErrMsg } = require('../src/Dimon'),
  AppServer = require('../src/Server');

describe('Dimon Server', function() {
  let env = {};
  const tmpShFile = tmp.fileSync();
  writeFileSync(tmpShFile.name, 'echo This is an action execution test!');

  const config = {
    port: 0, basePath: '',
    dimon: {
      menus: [
        {
          name: 'MENU_TEST',
          actions: [
            { name: "EXEC_ACTION", allowed: "always", actions: [
              { type: "exec", path: tmpShFile.name  }
            ] },
            { name: "SSH_ACTION", allowed: "never", actions: [] }
          ]
        }
      ],
      zones: [
        { name: 'DIMon_Zone', dns: '127.0.0.1:2510', servers: [
          { name: 'DIMON_TEST', display: 'DIMON TEST',
            description: 'Server for testing purposes',
            services: [ { name: 'SERVICE_1' }, { name: 'SERVICE_2' } ],
            actions: [
              { name: "EXEC_ACTION", allowed: "always", actions: [
                { type: "exec", path: tmpShFile.name }
              ] },
              { name: "IPMI_CONSOLE_ACTION", allowed: "expert", actions: [
                { type: "ipmi-console", host: "127.0.0.1" }
              ] },
              { name: "SSH_ACTION", allowed: "onWarning", actions: [
                { type: "ssh", host: "127.0.0.1", cmd: "ls" }
              ] },
              { name: "IPMI_RESET_ACTION", allowed: "onError", actions: [
                { type: "ipmi-reset", host: "127.0.0.1" }
              ] }
            ] }
        ] }
      ]
    }
  };

  beforeEach(async function() {
    replace(DnsClient, 'CONN_RETRY', 500);
    replace(DisClient, 'TIMEOUT', 500);

    env.dimDnsServer = new DnsServer('127.0.0.1', 2510);
    env.dimDis = new DisNode('127.0.0.1', 0,
      NodeInfo.local('127.0.0.1', 0, 'DIMON_TEST'));

    env.dimDis.addService('SERVICE_1', 'I', 1234);
    env.dimDis.addService('SERVICE_2', 'C', 'test');
    await env.dimDnsServer.listen();
    const dnsClient = new DnsClient('127.0.0.1', 2510);
    await dnsClient.register(env.dimDis);
    dnsClient.unref();

    env.appServer = new AppServer(cloneDeep(config));
    await env.appServer.listen();
  });

  afterEach(function() {
    if (get(env.appCli, 'readyState') === WebSocket.OPEN) {
      env.appCli.close();
    }
    invoke(env.dimDis, 'close');
    invoke(env.dimDnsServer, 'close');
    invoke(env.appServer, 'close');
    env = {};

    restore();
  });

  it('can monitor DIM services status', async function() {
    const addr = env.appServer.address();
    env.appCli =
      new WebSocket(`ws://127.0.0.1:${addr.port}/monitoring`);

    // wait for menus
    await waitForValue(env.appCli, 'message', (event) => event.data,
      JSON.stringify({ menus: config.dimon.menus }));

    let data = await waitFor(env.appCli, 'message', (event) => event.data);

    /* updated zones */
    const expectedZones = cloneDeep(config.dimon.zones);
    const server = expectedZones[0].servers[0];
    server.status = Status.OK;
    server.messages = {};
    server.services[0].available = true;
    server.services[1].available = true;

    expect(JSON.parse(data)).to.be.deep.equal({ zones: expectedZones });

    /* remove service 'SERVICE_2' */
    const prom = waitFor(env.appCli, 'message', (event) => event.data);
    env.dimDis.removeService('SERVICE_2');

    data = await prom;

    server.services[1].available = false;
    server.status = Status.WARNING;

    expect(JSON.parse(data)).to.be.deep.equal({ zones: expectedZones });
  });

  it('can monitor DNS Server status', async function() {
    const addr = env.appServer.address();
    env.appCli =
      new WebSocket(`ws://127.0.0.1:${addr.port}/monitoring`);

    // wait for menus
    await waitForValue(env.appCli, 'message', (event) => event.data,
      JSON.stringify({ menus: config.dimon.menus }));

    let data = await waitFor(env.appCli, 'message', (event) => event.data);
    let server = JSON.parse(data).zones[0].servers[0];
    expect(server.status).to.be.equal(Status.OK);

    // disable DNS server
    let prom = waitFor(env.appCli, 'message', (event) => event.data);
    env.dimDnsServer.close();

    data = await prom;

    server = JSON.parse(data).zones[0].servers[0];
    expect(server.status).to.be.equal(Status.ERROR);
    expect(server.messages)
    .to.have.own.property('DNS_DISCONNECTED', ErrMsg.DNS_DISCONNECTED);

    // re-enable DNS server
    prom = waitFor(env.appCli, 'message', (event) => event.data);
    env.dimDnsServer.listen();

    data = await prom;

    server = JSON.parse(data).zones[0].servers[0];
    expect(server.status).to.be.equal(Status.OK);
    expect(server.messages).to.be.deep.equal({});
  });

  it('can detect DNS server reachability', async function() {
    const addr = env.appServer.address();
    env.appCli =
      new WebSocket(`ws://127.0.0.1:${addr.port}/monitoring`);

    // disable DNS server and restart Dimon monitoring
    env.dimDnsServer.close();
    env.appServer.dimonService.dimon.monitor();

    // wait for menus
    await waitForValue(env.appCli, 'message', (event) => event.data,
      JSON.stringify({ menus: config.dimon.menus }));

    const data = await waitFor(env.appCli, 'message', (event) => event.data);
    const server = JSON.parse(data).zones[0].servers[0];
    expect(server.status).to.be.equal(Status.ERROR);
    expect(server.messages)
    .to.have.own.property('DNS_UNREACHABLE', ErrMsg.DNS_UNREACHABLE);
  });

  it('can monitor DIM server status', async function() {
    const addr = env.appServer.address();
    env.appCli =
      new WebSocket(`ws://127.0.0.1:${addr.port}/monitoring`);

    // wait for menus
    await waitForValue(env.appCli, 'message', (event) => event.data,
      JSON.stringify({ menus: config.dimon.menus }));

    let data = await waitFor(env.appCli, 'message', (event) => event.data);
    let server = JSON.parse(data).zones[0].servers[0];
    expect(server.status).to.be.equal(Status.OK);

    // disable DIM Server Provider
    let prom = waitFor(env.appCli, 'message', (event) => event.data);
    env.dimDis.close();
    env.dimDis = null;

    data = await prom;

    server = JSON.parse(data).zones[0].servers[0];
    expect(server.status).to.be.equal(Status.ERROR);
    expect(server.messages)
    .to.have.own.property('SERVER_DISCONNECTED', ErrMsg.SERVER_DISCONNECTED);

    // re-enable DIM Server Provider
    prom = waitFor(env.appCli, 'message', (event) => event.data);
    env.dimDis = new DisNode('127.0.0.1', 0,
      NodeInfo.local('127.0.0.1', 0, 'DIMON_TEST'));
    env.dimDis.register(env.dimDnsServer.url());

    data = await prom;

    server = JSON.parse(data).zones[0].servers[0];
    expect(server.status).to.be.equal(Status.WARNING);
    // SERVICE_1 and SERVICE_2 not added
    expect(server.services[0].available).to.be.equal(false);
    expect(server.services[1].available).to.be.equal(false);
    expect(server.messages).to.be.deep.equal({});
  });

  it('can serve ping requests', function(done) {
    const port = env.appServer.address().port;
    let buff = '';
    http.get(`http://127.0.0.1:${port}/ping?host=127.0.0.1`,
      (res) => {
        expect(res.statusCode).to.be.equal(200);

        res
        .on('error', done)
        .on('data', (chunk) => { buff += chunk; })
        .on('end', () => {
          expect(buff).to.be.equal(JSON.stringify({ reachable: true }));
          done();
        });
      });
  });

  describe('can serve action requests:', function() {
    const tests = [
      { title: 'action fired from a menu',
        req: { menu: 'MENU_TEST', name: 'EXEC_ACTION' },
        res: 'This is an action execution test!' },
      { title: 'action fired from a server',
        req: { zone: 'DIMon_Zone', server: 'DIMON_TEST', name: 'EXEC_ACTION' },
        res: 'This is an action execution test!' }
    ];

    tests.forEach((test) => {
      it(`${test.title}`, async function() {
        const addr = env.appServer.address();
        env.appCli =
          new WebSocket(`ws://127.0.0.1:${addr.port}/action`);

        await waitFor(env.appCli, 'open', stubTrue);

        const prom = waitFor(env.appCli, 'message', (event) => event.data);
        env.appCli.send(JSON.stringify(test.req));
        const output = await prom;

        expect(output).to.contain(test.res);
      });
    });
  });

  describe('raises an error when:', function() {
    const tests = [
      { title: 'requested action does not exist',
        req: { zone: 'DIMon_Zone', server: 'DIMON_TEST', name: 'XXX' },
        err: 'Action not found' },
      { title: 'requested action is not allowed due to missing expert role',
        req: { zone: 'DIMon_Zone', server: 'DIMON_TEST',
          name: 'IPMI_CONSOLE_ACTION' },
        err: 'Action requires the expert role' },
      { title: 'requested action is NEVER allowed',
        req: { menu: 'MENU_TEST', name: 'SSH_ACTION' },
        err: 'Action not allowed' },
      { title: `received 'onError' action does not comply with server status`,
        req: { zone: 'DIMon_Zone', server: 'DIMON_TEST',
          name: 'IPMI_RESET_ACTION' },
        err: 'Action allowed on server error status' },
      { title: `received 'onWarning' action does not comply with server status`,
        req: { zone: 'DIMon_Zone', server: 'DIMON_TEST',
          name: 'SSH_ACTION' },
        err: 'Action allowed on server warning status' }
    ];

    tests.forEach((test) => {
      it(`${test.title}`, async function() {
        const addr = env.appServer.address();
        env.appCli =
          new WebSocket(`ws://127.0.0.1:${addr.port}/action`);

        await waitFor(env.appCli, 'open', stubTrue);

        if (test.req.zone && test.req.server) {
          const ds = env.appServer.dimonService;

          await waitFor(ds.dimon, 'update', stubTrue, 2000);
          const server = ds.getServer(test.req.zone, test.req.server);

          expect(server.status).to.be.equal(Status.OK);
        }

        const prom = waitFor(env.appCli, 'close', (event) => event.reason);
        env.appCli.send(JSON.stringify(test.req));
        const error = await prom;

        expect(error).to.be.equal(test.err);
      });
    });
  });
});
