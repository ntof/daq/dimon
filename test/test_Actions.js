// @ts-check

const
  { expect } = require('chai'),
  { after, before, describe, it } = require('mocha'),
  sinon = require('sinon'),
  tmp = require('tmp'),
  { writeFileSync } = require('fs'),

  { waitFor, waitForValue } = require('./utils'),
  { ActionRunner } = require('../src/Actions/ActionRunner'),
  IPMIReset = require('../src/Actions/IPMIReset'),
  { SSHCmd, Systemctl } = require('../src/Actions/SSHCmd'),
  PLC = require('../src/Actions/PLC');

describe('Actions', function() {
  const env = {};

  before(function() {
    env.BashScriptFile = tmp.fileSync();
    env.stdoutMsg = 'TEST MESSAGE';
    env.stderrMsg = 'ERROR MESSAGE';

    writeFileSync(env.BashScriptFile.name,
      `echo -n ${env.stdoutMsg} && { >&2 echo -n ${env.stderrMsg}; }`);

    env.credentials = {
      'ipmi://localhost': { user: 'admin', password: 'pass' },
      'ssh://localhost': { user: 'root', key: 'XXX' }
    };
  });

  after(function() {
    delete env.stdoutMsg;
    delete env.stderrMsg;
    delete env.tmpShFile;
    delete env.credentials;
  });


  it('Bash script', async function() {
    let stdout = '', stderr = '';
    const ar = new ActionRunner(env.credentials);

    ar.on('stdout', (/** @type {string} */chunk) => { stdout += chunk; });
    ar.on('stderr', (/** @type {string} */chunk) => { stderr += chunk; });

    await ar.run({
      name: 'Bash Script test', allowed: 'always',
      actions: [ { type: 'exec', path: env.BashScriptFile.name } ]
    });

    ar.removeAllListeners();

    expect(stdout).to.be.equal(env.stdoutMsg);
    expect(stderr).to.be.equal(env.stderrMsg);
  });

  it('IPMI Reset', async function() {
    const ar = new ActionRunner(env.credentials);

    const execStub = sinon.stub(IPMIReset.prototype, 'exec');
    execStub.callsFake(async function() {
      // skip remote hard-reset command execution
      return this.return;
    });

    const prom = waitFor(ar, 'return', (data) => data);

    await ar.run({
      name: 'IPMI reset test', allowed: 'always',
      actions: [ { type: 'ipmi-reset', host: 'localhost' } ]
    });

    expect(await prom).to.contain('Reset done!');

    execStub.restore();
  });

  it('OpenShift Client Rollout (token missing)', async function() {
    const ar = new ActionRunner(env.credentials);

    await ar.run({
      name: 'OC rollout test', allowed: 'always',
      actions: [ { type: 'oc-rollout', host: 'localhost', namespace: 'ns',
        deployment: 'dp' } ]
    })
    .then(
      () => expect.fail('should fail'),
      (err) => expect(err.message)
      .to.be.equal("Failed to restart 'ns/dp': token missing"));
  });

  it('Ping', async function() {
    const ar = new ActionRunner(env.credentials);

    const prom = waitForValue(ar, 'return', (data) => data, 0); // Exit code 0

    await ar.run({
      name: 'Ping', allowed: 'always',
      actions: [ { type: 'ping', host: '127.0.0.1' } ]
    });

    await prom;
  });

  it('SSH Command', async function() {
    const ar = new ActionRunner(env.credentials);

    const execStub = sinon.stub(SSHCmd.prototype, 'exec');
    execStub.callsFake(async function() {
      // skip (remote) ssh command execution
      return this.return;
    });

    const prom = waitFor(ar, 'return', (data) => data);

    await ar.run({
      name: 'SSH Command', allowed: 'always',
      actions: [ { type: 'ssh', host: 'localhost', cmd: 'echo ssh test' } ]
    });

    expect(await prom).to.be.equal('Command performed.');

    execStub.restore();
  });

  it('Systemctl restart', async function() {
    const ar = new ActionRunner(env.credentials);

    const execStub = sinon.stub(Systemctl.prototype, 'exec');
    execStub.callsFake(async function() {
      // skip (remote) service restart execution
      return this.return;
    });

    const prom = waitFor(ar, 'return', (data) => data);

    await ar.run({
      name: 'SSH Command', allowed: 'always',
      actions: [ { type: 'systemctl', host: 'localhost', service: 'service' } ]
    });

    expect(await prom).to.be.equal('Service restarted.');

    execStub.restore();
  });

  it('PLC command', async function() {
    const ar = new ActionRunner(env.credentials);

    const execStub = sinon.stub(PLC.prototype, 'exec');
    execStub.resolves('PLC command performed.');

    const prom = waitFor(ar, 'return', (data) => data);

    await ar.run({
      name: 'PLC Command', allowed: 'always',
      actions: [ { type: 'plc', host: 'localhost', cmd: 'DB2,B0.2' } ]
    });

    expect(await prom).to.be.equal('PLC command performed.');

    execStub.restore();
  });
});
