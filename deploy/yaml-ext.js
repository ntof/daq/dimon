#!/usr/bin/env node

const { toString, trim } = require('lodash'),
  { execFileSync } = require('child_process'),
  fs = require('fs'),
  cmd = require('commander'),
  yaml = require('js-yaml');

class PasswordTag {
  constructor(pass, resolve = PasswordTag.RESOLVE) {
    this.pass = pass;
    this.resolve = resolve;
  }

  get() {
    return trim(toString(execFileSync('pass', [ this.pass ])));
  }
}
PasswordTag.RESOLVE = true;

const PasswordType = new yaml.Type('!pass', {
  kind: 'scalar',
  represent: (pass) => (pass.resolve ? pass.get() : pass.pass),
  multi: true,
  representName: function(pass) {
    return pass.resolve ? null : this.tag;
  },
  instanceOf: PasswordTag,
  construct: function(data) {
    return new PasswordTag(data);
  }
});

const schema = yaml.DEFAULT_SCHEMA.extend([ PasswordType ]);

function preload(data) {
  return yaml.dump(yaml.load(data, { schema }), { schema });
}

if (!module.parent) {
  const args = cmd.command('yaml-prepare')
  .argument('<file>', 'file to preprocess')
  .option('--no-pass', 'do not resolve password tags')
  .parse(process.argv);

  const opts = args.opts();
  PasswordTag.RESOLVE = opts.pass;

  process.on('unhandledRejection', function(reason) {
    console.warn(reason);
    process.exitCode = 1;
  });

  console.log(preload(fs.readFileSync(args.processedArgs[0])));
}
else {
  module.exports = { preload, schema, PasswordTag, PasswordType };
}
