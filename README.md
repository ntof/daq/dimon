# n_ToF DIMon service monitoring tool

A tool meant to monitor DIM services status and perform actions such as:
1. execute Bash scripts;
2. execute commands on a remote host;
3. restart remote services;
4. power on or hard-reset a remote machine;
5. launch a remote console;
6. restart Web Applications,
7. execute PLC commands (data read/write operations),
8. ping remote hosts.

This WebApp is developped by using [Base Website Template](https://gitlab.cern.ch/mro/common/www/base-website-template).

# Configuration

DIM services to be monitored and actions that can be performed are defined in the _config*.yml_ files.

Below is the configuration info:
```ts
interface dimon {
  // parameter to throttle monitoring updates in milliseconds (default: not throttling)
  trottle: number,
  // app menus containing generic actions
  menus: Array<{
    // menu name displayed in the UI
    name: string,
    actions: Array<{
      // action name displayed in the UI
      name: string,
      // permission to perform: [ 'always', 'expert', 'never' ]
      allowed: string,
      // list of sub-actions to perform (see below)
      action: Array<BashAction | SSHAction | SystemctlAction |
        IPMIResetAction | IPMIConsoleAction | OCRolloutAction |
        PLCAction | PingAction>
    }>
  }>,
  // monitoring info
  zones: Array<{
    // zone name displayed in the UI
    name: string,
    // DNS server to query
    dns: string,
    // DIM servers list
    servers: Array<{
      // DIM server name
      name: string,
      // server name displayed in the UI
      display: string,
      // description displayed in the UI hovering the server name
      description?: string,
      // DIM services list
      services: Array<{
        // DIM service name
        name: string
      }>,
      // Server-related actions list
      actions: Array<{
        // action name displayed in the UI
        name: string,
        // permission to perform: [ 'always', 'expert', 'never', 'onWarning', 'onError' ]
        allowed: string,
        // list of sub-actions to perform (see below)
        action: Array<BashAction | SSHAction | SystemctlAction |
          IPMIResetAction | IPMIConsoleAction | OCRolloutAction |
          PLCAction | PingAction>
    }>
  }>,
  credentials: {
    [ hostURL: string ] : { // e.g. 'ssh://xxx.cern.ch', 'ipmi://xxx.cern.ch')
      user?: string,
      key?: string, // ssh private key
      path?: string, // path of ssh private key
      password?: string, // IPMI password
      token?: string // bearer token (OpenShift API authentication)
    }
  }
}

// Actions
interface BashAction { type: 'exec', path: string }
interface SSHAction { type: 'ssh', host: string, cmd: string }
interface PingAction { type: 'ping', host: string }
interface SystemctlAction { type: 'systemctl', host: string, service: string }
interface IPMIResetAction { type: 'ipmi-reset', host: string, computer: string }
interface IPMIConsoleAction { type: 'ipmi-console', host: string }
interface OCRolloutAction { type: 'oc-rollout', namespace: string, deployment: string }
interface PLCAction { type: 'plc', host: string, rack?: number, slot?: number,
  cmd: string | { address: string | string[], value?: string | number | boolean } |
    Array<{ address: string | string[], value?: string | number | boolean } } // (*)
```

(*) For details about _address_ values see 'https://github.com/plcpeople/nodeS7#readme'. Following are some examples of PLC commands:
  - `cmd: 'DB2,INT0'`, to read an INT value from DB Area 2 at offset 0;
  - `cmd: { address: 'DB1,REAL0.2' }`, to read 2 REAL values in DB Area 1;
  - `cmd: { address: 'DB2,W0.1', value: 3 }`, to write value 3 into DBArea 2;
  - `cmd: { address: [ 'DB0,I0', 'DB4,I6' ]}`, to read several values at once;
  - `cmd: [ { address: 'DB5,S0.15' }, { address: 'DB3,B0.2', value: 0 } ]`, for a read/write operation sequence.

# Documentation

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Deploy

To deploy the example application, assuming that oc is configured and connected
on the proper project:
```bash
cd deploy

# Make sure the auth password referred to is correct
vim config-pass.yml

# Generate configuration from config*.yml files and apply it to a pod
./genconf.js dimon-beta.yaml config* | oc apply -f -
```
