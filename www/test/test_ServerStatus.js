// @ts-check

import './karma_index';
import { stubs, waitForWrapper } from './utils';
import { clone, map, values } from 'lodash';

import { afterEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import { expect } from 'chai';

import ServerStatus from '../src/components/ServerStatus.vue';


describe('Server Status', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('diplays server info properly', async function() {
    /** @type {AppServer.Dimon.Server} */
    const server = {
      name: 'ServerName', display: 'Server Name',
      description: 'Server for testing purpose',
      services: [ { name: "service_1" }, { name: "service_2" } ],
      messages: {},
      actions: []
    };

    /** @type {Tests.Wrapper} */
    wrapper = mount(ServerStatus, { propsData: { server }, stubs });

    const serverStatus = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'ServerStatus' }));

    // services
    const services = serverStatus.findAll('.x-service-name');
    /** @type {string[]} */let displayed = [];
    services.wrappers.forEach((service) => {
      displayed.push(service.element.innerHTML);
    });
    let expected = map(server.services, 'name');
    expect(displayed).to.have.members(expected);

    // messages (before update)
    let messages = serverStatus.findAll('ul.x-messages li');
    expect(messages.wrappers.length).to.be.equal(0);

    // actions (before update)
    let actionBtn = serverStatus.findComponent({ name: 'ActionList' });
    expect(actionBtn.exists()).to.be.false();

    // update
    const update = clone(server); // new obj for triggering reactivity
    update.messages = { msg1: 'message_1', msg2: 'message_2' };
    update.actions = [
      { name: 'action 1', allowed: 'always', actions: [
        { type: 'exec', path: '~/test.sh' }
      ] },
      { name: 'action 2', allowed: 'always', actions: [
        { type: "systemctl", host: "localhost", service: "test" }
      ] }
    ];
    await wrapper.setProps({ server: update });

    // messages (after update)
    messages = serverStatus.findAll('ul.x-messages li');
    displayed = [];
    messages.wrappers.forEach((message) => {
      displayed.push(message.element.innerHTML);
    });
    expected = values(update.messages);
    expect(displayed).to.have.members(expected);

    // actions (after update)
    actionBtn = serverStatus.findComponent({ name: 'ActionList' });
    expect(actionBtn.exists()).to.be.true();
    const actions = actionBtn.findAll('.x-action');
    displayed = [];
    actions.wrappers.forEach((action) => {
      displayed.push(action.element.innerHTML);
    });
    expected = map(update.actions, 'name');
    expect(displayed).to.have.members(expected);
  });
});
