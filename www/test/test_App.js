// @ts-check

import './karma_index';
import { createLocalVue, createRouter, waitFor,
  waitForValue, waitForWrapper } from './utils';
import { butils } from '@cern/base-vue';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import App from '../src/App.vue';
import store from '../src/store';
import { dimon as dimonConf } from '../../src/config-stub';
import { filter, find, includes, map, reduce, some } from 'lodash';

/**
 * @param {Tests.Wrapper[]} wrappers
 * @return {string[]}
 */
function getInnerHtml(wrappers) {
  return reduce(wrappers,
    function(/** @type {string[]} */list, /** @type {Tests.Wrapper} */item) {
      list.push(item.element.innerHTML);
      return list;
    }, []);
}


describe('App', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        var addr = this.env.server.address();

        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      butils.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
    store.commit('update', { user: { "cern_roles": [] } });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('displays connection status with server side', async function() {
    wrapper = mount(App, {
      router: createRouter(),
      localVue: createLocalVue({ auth: true })
    });

    await waitForValue(wrapper,
      () => wrapper.find('.x-connection-status').element.innerHTML, 'Connected');

    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
    });

    await waitForValue(wrapper,
      () => wrapper.find('.x-connection-status').element.innerHTML, 'Disconnected');
  });

  it('displays action menus properly', async function() {
    wrapper = mount(App, {
      router: createRouter(),
      localVue: createLocalVue({ auth: true })
    });

    const menus = await waitForWrapper(wrapper,
      () => wrapper.findAllComponents({ name: 'ActionList' }));
    expect(menus.length).to.be.equal(dimonConf.menus.length);

    menus.wrappers.forEach((menu, idx) => {
      expect(menu.props('title')).to.be.equal(dimonConf.menus[idx].name);

      // check actions drop-down list (no EXPERT role)
      const expectedActs = map(filter(dimonConf.menus[idx].actions,
        [ 'allowed', 'always' ]), 'name');

      /** @type {string[]} */
      const menuActs = getInnerHtml(menu.findAll('.dropdown-item').wrappers);
      expect(menuActs).to.have.members(expectedActs);
    });

    // Set EXPERT role
    store.commit('update', { user: { "cern_roles": [ 'expert' ] } });
    await waitFor(wrapper, () => store.getters.isExpert);

    menus.wrappers.forEach((menu, idx) => {
      expect(menu.props('title')).to.be.equal(dimonConf.menus[idx].name);

      const expectedActs = map(dimonConf.menus[idx].actions, 'name');

      /** @type {string[]} */
      const menuActs = getInnerHtml(menu.findAll('.dropdown-item').wrappers);
      expect(menuActs).to.have.members(expectedActs);
    });
  });

  it('displays zones properly', async function() {
    wrapper = mount(App, {
      router: createRouter(),
      localVue: createLocalVue({ auth: true })
    });

    const zones = await waitForWrapper(wrapper,
      () => wrapper.findAll('.x-zone'));
    expect(zones.length).to.be.equal(dimonConf.zones.length);

    const findAllServerStatus =
      () => wrapper.findAllComponents({ name: 'ServerStatus' });
    const getStoredZone = () => store.state.zone;

    for (let i = 0; i < zones.length; i++) {
      const zoneWrap = zones.wrappers[i];
      const zoneConf = dimonConf.zones[i];

      expect(zoneWrap.element.innerHTML).to.contain(zoneConf.name);

      // select zone
      store.commit('queryChange', { zone: zoneConf.name });
      await waitForValue(wrapper, getStoredZone, zoneConf.name);

      // get server name list from configuration
      const expectedServers = map(zoneConf.servers, 'display');

      // check server list
      /** @type {string[]} */
      const serverList = getInnerHtml(wrapper.findAll('.x-server-name').wrappers);
      expect(serverList).to.have.members(expectedServers);

      // check service list per server
      const servers = await waitForWrapper(wrapper, findAllServerStatus);

      for (const srv of servers.wrappers) {
        const serverConf = find(zoneConf.servers,
          [ 'display', srv.find('.x-server-name').element.innerHTML ]) ||
          { services: [] };
        expect(serverConf.services).to.be.an('array').that.is.not.empty();

        const expectedServices = map(serverConf.services, 'name');
        const serviceList = getInnerHtml(srv.findAll('.x-service-name').wrappers);
        expect(serviceList).to.have.members(expectedServices);
      }
    }
  });

  it('displays monitoring source errors', async function() {
    wrapper = mount(App, {
      router: createRouter(),
      localVue: createLocalVue({ auth: true })
    });

    const ErrorAlert = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'BaseErrorAlert' }));
    expect(ErrorAlert.exists()).to.be.true();

    /* trigger server-side WebSocket closure */
    server.run(function() {
      // @ts-ignore
      this.env.server.dimonService.release();
    });

    await waitFor(ErrorAlert,
      () => some(ErrorAlert.findAll('.alert').wrappers,
        (e) => includes(e.element.innerHTML, 'WebSocket closed')));
  });

  it('logs monitoring source errors', async function() {
    wrapper = mount(App, {
      router: createRouter(),
      localVue: createLocalVue({ auth: true })
    });

    const ErrorReport = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'BaseErrorReport' }));
    expect(ErrorReport.exists()).to.be.true();

    /* trigger server-side WebSocket closure */
    server.run(function() {
      // @ts-ignore
      this.env.server.dimonService.release();
    });

    await waitFor(ErrorReport,
      () => some(ErrorReport.findAll('.text-danger').wrappers,
        (e) => includes(e.element.innerHTML, 'WebSocket closed')));
  });
});
