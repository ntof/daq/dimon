
import Vue from 'vue';
import { config as tuConfig } from '@vue/test-utils';
import { before } from 'mocha';

import {
  default as BaseVue,
  BaseLogger as logger } from "@cern/base-vue";

import store from '../src/store';

import d from 'debug';
const debug = d('test:error');

before(function() {
  Vue.use(BaseVue);

  /* "provide" doesn't seem to work properly, using mocks */
  tuConfig.mocks['$store'] = store;

  logger.on('error', (error) => debug('error:', error));
});
