// @ts-check

import './karma_index';
import { waitFor, waitForWrapper } from './utils';
import { butils } from '@cern/base-vue';
import { find } from 'lodash';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import { expect } from 'chai';
import server from '@cern/karma-server-side';

import ActionDialog from '../src/components/Actions/ActionDialog.vue';
import { dimon as dimonConf } from '../../src/config-stub';

/**
 * @param {string} zoneName
 * @param {string} serverName
 * @param {string} actionName
 * @return {AppServer.Dimon.Action | undefined}
 */
function getTestAction(zoneName, serverName, actionName) {
  const zone = find(dimonConf.zones, [ 'name', zoneName ]) ||
    { name: '', servers: [] };
  expect(zone.name).to.be.a('string').that.is.not.empty();
  expect(zone.servers).to.be.an('array').that.is.not.empty();

  const server = find(zone.servers, [ 'name', serverName ]) ||
    { name: '', actions: [] };
  expect(server.name).to.be.a('string').that.is.not.empty();
  expect(server.actions).to.be.an('array').that.is.not.empty();

  const action = find(server.actions, [ 'name', actionName ]);
  expect(action).to.be.not.undefined();
  return action;
}

describe('Actions', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        var addr = this.env.server.address();

        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      butils.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.stubs.off();
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  [ 'Bash Script', 'SSH Cmd', 'Systemctl', 'OC Rollout', 'IPMI Reset', 'PLC' ]
  .forEach((actionName) => {
    it(`${actionName}`, async function() {
      wrapper = mount(ActionDialog, {});

      /** @type {Tests.Wrapper} */
      const actDialog = await waitForWrapper(wrapper,
        () => wrapper.findComponent({ name: 'ActionDialog' }));

      // @ts-ignore
      await server.run(function() { this.env.stubs.on(); });

      const act = getTestAction('EAR2', 'DIS_DNS_1', `${actionName} test`);

      actDialog.vm.runAction(act, { zone: 'EAR2', server: 'DIS_DNS_1' });

      // wait for the action to end (i.e. ok button enabled)
      await waitFor(actDialog,
        () => !actDialog.find('#x-ok-btn').element.hasAttribute('disabled'));

      const outputs = actDialog.findAll('.x-output');
      expect(outputs.wrappers.length).to.be.equal(1);
      expect(outputs.wrappers[0].element.innerHTML).to.be.equal('DONE');
    });
  });

  it('IPMI Console', async function() {
    wrapper = mount(ActionDialog, {});

    /** @type {Tests.Wrapper} */
    const actDialog = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'ActionDialog' }));

    const act = getTestAction('EAR2', 'DIS_DNS_1', `IPMI Console test`);

    actDialog.vm.runAction(act, { zone: 'EAR2', server: 'DIS_DNS_1' });

    // wait for the action to end (i.e. ok button enabled)
    await waitFor(actDialog,
      () => !actDialog.find('#x-ok-btn').element.hasAttribute('disabled'));

    const link = actDialog.findAll('.x-link');
    expect(link.wrappers.length).to.be.equal(1);
    expect(link.wrappers[0].element.innerHTML).to.contain('Remote Console');
  });

  it('Ping', async function() {
    wrapper = mount(ActionDialog, { attachTo: document.body });

    /** @type {Tests.Wrapper} */
    const actDialog = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'ActionDialog' }));

    const act = getTestAction('EAR2', 'DIS_DNS_1', `Ping test`);

    actDialog.vm.runAction(act, { zone: 'EAR2', server: 'DIS_DNS_1' });

    // wait for the action to end (i.e. ok button enabled)
    await waitFor(actDialog,
      () => !actDialog.find('#x-ok-btn').element.hasAttribute('disabled'));

    const output = actDialog.findAll('.x-output');
    expect(output.wrappers.length).to.be.equal(1);
    expect(output.wrappers[0].element.innerHTML)
    .to.be.equal("'localhost' reachable");
  });
});
