const
  { makeDeferred } = require('@cern/prom'),
  sinon = require('sinon'),

  Server = require('../../src/Server'),
  { ActionRunner } = require('../../src/Actions/ActionRunner'),
  dimonConf = require('../../src/config-stub').dimon;

/**
 * @typedef {import('events').EventEmitter} EventEmitter
 */

/**
 * @param  {any} env
 * @return {Promise<void>}
 */
function createApp(env) {
  /** @type {prom.Deferred<void>} */
  var def = makeDeferred();

  env.server = new Server({
    port: 0, basePath: '',
    dimon: dimonConf
  });

  env.stubs = (function() {
    /** @type {sinon.SinonStub | null} */
    let stub = null;
    return {
      on: () => {
        if (!stub) {
          stub = sinon.stub(ActionRunner.prototype, 'run');
          stub.callsFake(
            /** @this {EventEmitter} */
            async function() {
              this.emit('return', 'DONE');
              return true;
            });
        }
      },
      off: () => {
        if (stub) { stub.restore(); }
      }
    };
  }());

  env.server.listen(() => def.resolve());
  return def.promise;
}

module.exports = { createApp };
