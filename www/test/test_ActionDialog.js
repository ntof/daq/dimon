// @ts-check

import './karma_index';
import { waitFor, waitForWrapper } from './utils';
import { butils } from '@cern/base-vue';
import $ from 'jquery';
import { find, isEqual } from 'lodash';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { expect } from 'chai';

import ActionDialog from '../src/components/Actions/ActionDialog.vue';
import { dimon as dimonConf } from '../../src/config-stub';
import { Status } from '../src/interfaces';


describe('Action Dialog', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');

      this.env = /** @type {any} */({});
      return utils.createApp(this.env)
      .then(() => {
        var addr = this.env.server.address();

        return {
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((/** @type {any} */ ret) => {
      butils.setCurrentUrl(ret.proxyUrl);
    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can request an action and display the output', async function() {
    wrapper = mount(ActionDialog, {});

    /** @type {Tests.Wrapper} */
    const actDialog = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'ActionDialog' }));

    const menu = find(dimonConf.menus, [ 'name', 'Monitoring' ]) ||
      { name: '', actions: [] };
    expect(menu.name).to.be.a('string').that.is.not.empty();
    expect(menu.actions).to.be.an('array').that.is.not.empty();

    const action = find(menu.actions, [ 'name', 'Action test' ]);
    expect(action).to.be.not.undefined();

    actDialog.vm.runAction(action, { menu: menu.name });

    // wait for the action to end (i.e. ok button enabled)
    const okBtn = actDialog.find('#x-ok-btn');
    await waitFor(actDialog,
      () => !okBtn.element.hasAttribute('disabled'));
    expect(okBtn.element.innerHTML).to.be.equal('Ok');

    const errors = actDialog.findAll('.x-error');
    expect(errors.wrappers.length).to.be.equal(1);
    expect(errors.wrappers[0].element.innerHTML).to.be.equal('Command missing');
  });

  it('can ask for confirmation', async function() {
    wrapper = mount(ActionDialog, {});

    /** @type {Tests.Wrapper} */
    const actDialog = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'ActionDialog' }));

    const zone = find(dimonConf.zones, [ 'name', 'EAR1' ]) ||
      { name: '', servers: [] };
    expect(zone.name).to.be.a('string').that.is.not.empty();
    expect(zone.servers).to.be.an('array').that.is.not.empty();

    const server = find(zone.servers, [ 'name', 'DIS_DNS_1' ]) ||
      { name: '', actions: [] };
    expect(server.name).to.be.a('string').that.is.not.empty();
    expect(server.actions).to.be.an('array').that.is.not.empty();

    const action = find(server.actions, [ 'name', 'Critical action test' ]);
    expect(action).to.be.not.undefined();

    actDialog.vm.runAction(action, { zone: zone.name, server: server.name },
      Status.OK);

    const okBtn = actDialog.find('#x-ok-btn');
    await waitFor(actDialog,
      () => isEqual(okBtn.element.innerHTML, 'Confirm'));
    expect(actDialog.find('#x-cancel-btn').exists()).to.be.true();

    expect(actDialog.find('.x-dialog-body').element.innerHTML)
    .to.contain('Are you sure you want to perform this action?');
  });

  it('prevents closure during action execution', async function() {
    wrapper = mount(ActionDialog, { attachTo: document.body });

    /** @type {Tests.Wrapper} */
    const actDialog = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'ActionDialog' }));
    const okBtn = actDialog.find('#x-ok-btn');

    let isOpen = false;
    expect(actDialog.vm.isRunning).to.be.false();
    $(actDialog.element).one('shown.bs.modal', () => { isOpen = true; });
    $(actDialog.element).one('hide.bs.modal', () => { isOpen = false; });

    /* simulate action execution */
    // - start execution
    actDialog.vm.isRunning = true;
    let prom = waitFor(wrapper, () => isOpen);
    const ret = actDialog.vm.$refs.dialog.request();
    await prom;

    // - try to close dialog by clicking on 'Ok' button
    expect(okBtn.element.hasAttribute('disabled')).to.be.true();
    await okBtn.trigger('click');
    expect(isOpen).to.be.true();

    // - try to close dialog by pressing 'Esc' key
    await actDialog.trigger('keydown.esc');
    expect(isOpen).to.be.true();

    // - try to close dialog by clicking outside it
    let clickOutside = false;
    $(actDialog.element).one('hidePrevented.bs.modal',
      () => { clickOutside = true; });
    expect($('.modal-backdrop').length).to.be.equal(1); // exists a backdrop

    prom = waitFor(wrapper, () => clickOutside);
    $(actDialog.element).trigger('click'); // fires a dismiss click
    await prom;
    expect(isOpen).to.be.true();

    // - terminate execution
    actDialog.vm.isRunning = false;
    await waitFor(wrapper, () => !okBtn.element.hasAttribute('disabled'));
    okBtn.trigger('click');
    expect(await ret).to.be.true();
    await waitFor(wrapper, () => !isOpen);
  });
});
