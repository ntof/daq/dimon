const
  { karmaConfig } = require('@cern/karma-mocha-webpack');

module.exports = function(karma) {
  karma.set(karmaConfig(karma));
};
