/* eslint-disable vue/one-component-per-file */
// @ts-check
import Vue from 'vue';
import { assign, get, has, isString, lowerFirst, toNumber } from 'lodash';
import { DicXmlDataSet } from '@ntof/redim-client';

/* NOTE: place your utility functions here */

let id = 0;
/**
* @return {string}
*/
export function genId() {
  return 'a-' + (++id);
}

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}

/**
 * @param  {string} dns DIM dns hostname
 * @return {string} dns proxy url
 */
export function dnsUrl(dns) {
  return _currentUrl + '/' + dns + '/dns/query';
}

export const UrlUtilitiesMixin = Vue.extend({
  methods: {
    getProxy() {
      return _currentUrl;
    },
    currentUrl() {
      return _currentUrl;
    },
    /**
     * @param  {string} dns
     */
    dnsUrl(dns/*: any */) {
      return _currentUrl + '/' + dns + '/dns/query';
    }
  }
});

/**
 * @param  {string} prefix string to append on errors
 * @param  {string|Error} err
 * @return {string|Error}
 */
export function errorPrefix(prefix, err) {
  if (isString(err)) {
    return prefix + err;
  }
  else if (has(err, 'message')) {
    err.message = prefix + lowerFirst(err.message);
  }
  throw err;
}

/**
 * @details this mixin can be used when ParamInput refs matches the description
 * @typedef {V.Instance<typeof ParamBuilderMixin>} Instance
 */
export const ParamBuilderMixin =  Vue.extend({
  methods: {
    /**
     * [paramBuilder description]
     * @param {string} name name of the param
     * @param {any} value current "live" value for the param
     * @param {{ type: ParamType } & Object<string, any>} desc param description
     * @param {Array<any>} params output values
     */
    paramBuilder(name, value, desc, params) {
      const editValue = parseValue(get(this.$refs, [ name, 'editValue' ]), desc.type);
      if (value !== editValue) {
        params.push(assign({ value: editValue }, desc));
      }
    }
  }
});

// @ts-ignore
export const ParamType = DicXmlDataSet.DataType;

/**
 * @param {string | number} value
 * @param {redim.XmlData.DataType} type
 * @return {boolean|number|string}
 */ // eslint-disable-next-line complexity
export function parseValue(value, type) {
  switch (type) {
  case ParamType.INT64:
  case ParamType.INT32:
  case ParamType.INT16:
  case ParamType.INT8:
  case ParamType.DOUBLE:
  case ParamType.FLOAT:
  case ParamType.ENUM:
  case ParamType.UINT32:
  case ParamType.UINT64:
  case ParamType.UINT8:
  case ParamType.UINT16:
    return toNumber(value);
  case ParamType.BOOL:
    return toNumber(value) > 0;
  default:
    return value;
  }
}

