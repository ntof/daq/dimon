// @ts-check
import Vue from 'vue';
import { mapGetters } from 'vuex';
import { Allowed, Status } from '../interfaces';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = Vue.extend({
  computed: {
    .../** @type {{ isExpert(): boolean }} */(mapGetters([ 'isExpert' ]))
  },
  methods: {
    /**
     * @param {string} allowed
     * @param {string} [serverStatus = '']
     * @returns {boolean}
     */
    isAllowed(allowed, serverStatus = '') { // eslint-disable-line complexity
      switch (allowed) {
      case Allowed.EXPERT:
        return this.isExpert ? true : false;
      case Allowed.ONERROR:
        return (serverStatus === Status.ERROR) ? true : this.isExpert;
      case Allowed.ONWARNING:
        return (serverStatus === Status.WARNING ||
                serverStatus === Status.ERROR) ? true : this.isExpert;
      case Allowed.ALWAYS:
        return true;
      case Allowed.NEVER:
      default:
        return false;
      }
    }
  }
});
export default component;
