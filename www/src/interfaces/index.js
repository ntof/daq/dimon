// @ts-check

export const Status = {
  OK: 'OK',
  WARNING: 'WARNING',
  ERROR: 'ERROR'
};

export const Allowed = {
  ALWAYS: 'always',
  NEVER: 'never',
  ONERROR: 'onError',
  ONWARNING: 'onWarning',
  EXPERT: 'expert'
};

export const ActType = {
  IPMI_CONSOLE: 'ipmi-console',
  PING: 'ping',
  IPMI_RESET: 'ipmi-reset',
  SSH: 'ssh',
  SYSTEMCTL: 'systemctl',
  PLC: 'plc',
  EXEC: 'exec',
  OC_ROLLOUT: 'oc-rollout'
};

/**
 * @param {string} url
 */
export function httpToWs(url) {
  return url.replace(/^http(s)?\:\/\//, "ws$1://");
}
