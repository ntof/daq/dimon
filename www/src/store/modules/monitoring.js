// @ts-check

import { assign } from "lodash";

/** @type {V.Module<AppStore.MonitoringState>} */
export default {
  namespaced: true,
  state: () => ({
    menus: [],
    zones: []
  }),
  mutations: {
    update: assign
  }
};
