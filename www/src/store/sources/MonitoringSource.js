// @ts-check

import { BaseLogger } from '@cern/base-vue';
import { get, invoke } from 'lodash';
import { httpToWs } from '../../interfaces';

export default class MonitoringSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store) {
    this.store = store;
    this.timerKeepAlive = null;
    this.ws = null;
  }

  /**
   * @param {string} url
   */
  connect(url) {
    const WsUrl = httpToWs(url);
    if (WsUrl !== get(this.store, 'url')) {
      this.close();
      this.store.commit('update', { url: WsUrl });
      this.ws = new WebSocket(WsUrl);

      this.ws.onerror = () => BaseLogger.error(`WebSocket error`);
      this.ws.onclose = (e) => {
        this.store.commit('update', { connected: false });
        BaseLogger.error(`WebSocket closed: ${e.reason} (code: ${e.code})`);
        this._clearTimer();
        this.store.commit('update', { url: null });
        this.ws = null;
      };
      this.ws.onopen = () => {
        this.store.commit('update', { connected: true });
        this.timerKeepAlive = setInterval(this.keepAlive.bind(this),
          MonitoringSource.KEEPALIVE_DELAY);
      };
      this.ws.onmessage = (event) => {
        if (event.data) {
          try {
            this.store.commit('monitoring/update', JSON.parse(event.data));
          }
          catch (err) {
            BaseLogger.error(`Failed to parse incoming data: ${err.message}`);
          }
        }
      };
    }
  }

  keepAlive() {
    invoke(this.ws, 'send', JSON.stringify(MonitoringSource.KEEPALIVE_PAYLOAD));
  }

  _clearTimer() {
    if (this.timerKeepAlive) {
      clearInterval(this.timerKeepAlive);
      this.timerKeepAlive = null;
    }
  }

  close() {
    if (!this.ws) { return; }
    this._clearTimer();
    this.ws.close(1000);
    this.store.commit('update', { url: null });
    this.ws = null;
  }
}

MonitoringSource.KEEPALIVE_DELAY = 10000;
MonitoringSource.KEEPALIVE_PAYLOAD = { keepAlive: true };
