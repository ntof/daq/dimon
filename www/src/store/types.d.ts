
import { Dimon } from '../../../src/types'

export = AppStore
export as namespace AppStore

declare namespace AppStore {
  interface State {
    url: string | null,
    zone: string | null,
    user: { username: string, [key: string]: any } | null
  }

  interface UiState {
    showKeyHints: boolean
  }

  interface MonitoringState {
    menus: Array<Dimon.Menu>,
    zones: Array<Dimon.Zone>
  }
}
