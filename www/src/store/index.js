// @ts-check

import { assign, get, includes, merge } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';
import {
  sources as baseSources,
  createStore,
  storeOptions } from '@cern/base-vue';

import monitoring from './modules/monitoring';
import MonitoringSource from './sources/MonitoringSource';

Vue.use(Vuex);

merge(storeOptions,
  /** @type {V.StoreOptions<AppStore.State & { connected: boolean }>} */({
    /* NOTE: declare your store and modules here */
    state: {
      url: null,
      zone: null,
      user: null,
      connected: false
    },
    getters: {
      isExpert: (state) => {
        const roles = get(state, [ 'user', 'cern_roles' ]);
        return includes(roles, 'expert');
      }
    },
    mutations: {
      queryChange(state, query) {
        state.zone = get(query, 'zone', null);
      },
      update: assign
    },
    modules: { monitoring }
  })
);

const store = /** @type {V.Store<AppStore.State>} */(createStore());
export default store;

export const sources = merge(baseSources, {
  /* NOTE: declare your global data-sources here */
  monitoring: new MonitoringSource(store)
});
