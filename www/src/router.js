// @ts-check
import Vue from 'vue';
import Router from 'vue-router';
import store from './store';

import ServerList from './components/ServerList.vue';
import AddhExtraLayout from './components/AddhExtra/AddhExtraLayout.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/', name: 'Home',
      component: ServerList,
      meta: { navbar: false }
    },
    {
      path: '/addh-extra', name: 'AddhExtra',
      component: AddhExtraLayout,
      meta: { navbar: false }
    },

    { path: '/index.html', redirect: '/', meta: { navbar: false } }
  ]
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.afterEach((to, from) => { /* jshint unused:false */
  store.commit('queryChange', to.query);
});

export default router;
