// @ts-check

import Vue from 'vue';
import { Status } from '../interfaces';
import ActionList from '../components/ActionList.vue';
import ActionDialog from './Actions/ActionDialog.vue';
import { filter, get, isEmpty } from 'lodash';
import ActionMixin from '../mixins/ActionMixin';

const statusClassMap = {
  [Status.OK]: 'success',
  [Status.WARNING]: 'warning',
  [Status.ERROR]: 'danger'
};

/**
 * @typedef {V.Instance<typeof component> & V.Instance<typeof ActionMixin>} Instance
 */

const component = Vue.extend({
  name: 'ServerStatus',
  components: { ActionList, ActionDialog },
  mixins: [ ActionMixin ],
  props: {
    server: { type: Object, default: null },
    zone: { type: String, default: '' }
  },
  data() {
    return { showDetails: false, actIdx: -1 };
  },
  computed: {
    /** @return {{ [key: string]: string }} */
    messages() { return get(this.server, 'messages', {}); },
    /** @return {string} */
    status() { return get(this.server, 'status'); },
    /** @return {boolean} */
    hasMsgs() { return !isEmpty(this.server.messages); },
    /**
     * @this {Instance}
     * @return {Array<AppServer.Dimon.Action>}
     */
    allowedActions() {
      return filter(get(this.server, 'actions', []),
        (act) => this.isAllowed(act.allowed, this.status));
    }
  },
  methods: {
    /**
     * @param {string} status
     * @return {string}
     */
    statusClass(status) {
      return get(statusClassMap, [ status ], 'secondary');
    },
    toggleDetails() {
      this.showDetails = !this.showDetails;
    }
  }
});
export default component;
