// @ts-check

import Vue from 'vue';
import { assign, get, invoke, isEmpty, noop, set } from 'lodash';
import { BaseDialog, BaseLogger as bl } from '@cern/base-vue';
import { makeDeferred } from '@cern/prom';
import { ActType, Allowed, httpToWs } from '../../interfaces';
import IPMIConsole from './IPMIConsole';
import Ping from './Ping';
import IPMIReset from './IPMIReset';
import BashScript from './BashScript';
import Systemctl from './Systemclt';
import SSHCmd from './SSHCmd';
import OCRollout from './OCRollout';
import PLC from './PLC';

/**
 * @typedef {{ dialog: V.Instance<BaseDialog> }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> & { $butils: BaseVue.butils }} Instance
 * @typedef {AppServer.Dimon.ActionInfo} ActionInfo
 * @typedef {import("./Action").ActionSource} ActionSource
 */

const WS_NORMAL_CLOSURE = 1000;
const WS_GOING_AWAY = 1001;

/** @type {{ [key: string]: Action }} */
const ActionSetUp = {
  [ActType.IPMI_CONSOLE]: IPMIConsole,
  [ActType.PING]: Ping,
  [ActType.IPMI_RESET]: IPMIReset,
  [ActType.EXEC]: BashScript,
  [ActType.SYSTEMCTL]: Systemctl,
  [ActType.SSH]: SSHCmd,
  [ActType.PLC]: PLC,
  [ActType.OC_ROLLOUT]: OCRollout
};

const options = {
  /** @type {prom.Deferred<boolean>|null} */
  _hidden: null
};

const component = /** @type {V.Constructor<typeof options, Refs>} */(Vue).extend({
  name: 'ActionDialog',
  ...options,
  /**
   * @return {{ title: string, warnings: string[],
   *  extLinks: Array<{ url: string, descr: string }>,
   *  output: Array<{ type: string, data: any }>,
   *  sendActReq: boolean, isRunning: boolean,
   *  confirmProm: Promise<boolean>|null
   *  ws: WebSocket|null }}
   */
  data() {
    return {
      title: '', warnings: [], output: [], extLinks: [],
      sendActReq: false, confirmProm: null, isRunning: false,
      ws: null
    };
  },
  computed: {
    /** @return {boolean} */ warning() { return !isEmpty(this.warnings); }
  },
  /** @this {Instance} */
  beforeDestroy() {
    if (this.$options._hidden) {
      this.$options._hidden.reject(new Error('destroyed'));
      this.$options._hidden = null;
    }

    if (this.ws && this.ws.readyState === WebSocket.OPEN) {
      this.ws.close(WS_GOING_AWAY);
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {AppServer.Dimon.Action} action
     * @param {ActionSource} source
     */
    async setup(action, source) {
      const actions = get(action, 'actions', []);
      if (isEmpty(actions)) { throw new Error('No action defined'); }

      for (const act of actions) {
        const Action = ActionSetUp[act.type];
        if (!Action) { throw new Error(`No such action [${act.type}]`); }
        const config =
          await Action.getConfig(assign(act, source, { name: action.name }));

        if (config.warning) { this.warnings.push(config.warning); }
        if (config.output) { this.output.push(config.output); }
        if (config.extLink) { this.extLinks.push(config.extLink); }

        this.sendActReq = this.sendActReq || Action.useWS();
      }

      if (isEmpty(this.warnings) && (action.allowed !== Allowed.ALWAYS)) {
        this.warnings = [
          'This action can affect other services and must be used with care!'
        ];
      }
    },
    clear() {
      if (this.$options._hidden) {
        this.$options._hidden.reject(new Error('aborted'));
        this.$options._hidden = null;
      }
      this.title = '';
      this.extLinks = [];
      this.warnings = [];
      this.output = [];
      this.sendActReq = this.isRunning = false;
      this.ws = null;
    },
    /** @param {boolean} hidden */
    onHidden(hidden) {
      if (hidden && this.$options._hidden) {
        this.$options._hidden.resolve(true);
        this.$options._hidden = null;
      }
    },
    /**
     * @this {Instance}
     * @param {AppServer.Dimon.Action} action
     * @param {ActionSource} source
     * @return {Promise<void>}
     */
    async runAction(action, source) {
      this.clear();

      this.title = action.name;

      try {
        await this.setup(action, source);
      }
      catch (err) {
        this.output.push({ type: 'stderr', data: err.message });
      }

      if (this.warning) {
        // display any warnings and ask for confirmation
        this.$options._hidden = makeDeferred();
        this.confirmProm = this.$refs.dialog.request();

        // wait for the dialogue to be completely hidden
        await this.$options._hidden.promise.catch(noop);
        this.warnings = [];
      }
      else {
        // skip confirmation
        this.confirmProm = Promise.resolve(true);
      }

      return this.confirmProm
      .then((confirmed) => {
        if (!confirmed) { return; }

        if (!this.sendActReq) {
          // open dialog directly
          this.$refs.dialog.request().catch(noop);
        }
        else {
          // send action request through WebSocket and open dialog
          const WsUrl = httpToWs(`${this.$butils.currentUrl()}/action`);
          this.ws = new WebSocket(WsUrl);
          this.ws.onerror = () => bl.error(`WebSocket error [${WsUrl}]`);
          this.ws.onclose = (e) => {
            this.isRunning = false;
            if (e.code !== WS_NORMAL_CLOSURE) {
              bl.error(`WebSocket closed: ${e.reason} (code: ${e.code})`);
              this.output.push({ type: 'error', data: e.reason });
            }
          };
          this.ws.onopen = () => {
            // @ts-ignore: 'ws' cannot be null here
            this.ws.send(JSON.stringify({ ...source, name: action.name }));
            this.isRunning = true;

            this.$refs.dialog.request()
            .catch(noop)
            .finally(() => {
              this.isRunning = false;
              set(this, [ 'ws', 'onclose' ], null);
              invoke(this.ws, 'close', WS_NORMAL_CLOSURE);
            });
          };
          this.ws.onmessage = (event) => {
            if (event.data) {
              try {
                this.output.push(JSON.parse(event.data));
              }
              catch (err) {
                bl.error(`Failed to parse incoming data: ${err.message}`);
              }
            }
          };
        }
      })
      .catch(noop);
    }
  }
});
export default component;
