export = Action;
export as namespace Action;

export type ActionConfig = Partial<{
  output: { type: string, data: any },
  warning: string,
  extLink: { url: string, descr: string }
}>;

export type ActionSource = Partial<{
  menu: string,
  zone: string,
  server: string
}>;

export type ExtendedActionInfo = AppServer.Dimon.ActionInfo &
  ActionSource & { name?: string };

declare class Action {
  getConfig(info?: ExtendedActionInfo): Promise<ActionConfig>;
  useWS(): boolean;
}
