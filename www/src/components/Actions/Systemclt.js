// @ts-check

/** @typedef {import("./Action").ActionConfig} ActionConfig */

class Systemctl {
  /**
   * @return {Promise<ActionConfig>}
   */
  static async getConfig() { return {}; } // no config

  /** @return {boolean} */
  static useWS() { return true; }
}

export default Systemctl;
