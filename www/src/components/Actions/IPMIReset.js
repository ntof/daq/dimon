// @ts-check

import { isReachable } from './Ping';

/**
 * @typedef {import("./Action").ActionConfig} ActionConfig
 */

class IPMIReset {

  /**
   * @param {AppServer.Dimon.IPMIResetAction} info
   * @return {Promise<ActionConfig>}
   */
  static async getConfig(info) {
    if (info.host && await isReachable(info.host)) {
      return { warning: `'${info.computer || info.host}' will be hard-reset. ` +
        'Resetting it while it is still alive can break the hard-disk!' };
    }
    return {};
  }

  /** @return {boolean} */
  static useWS() { return true; }
}


export default IPMIReset;
