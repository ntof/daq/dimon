// @ts-check

/** @typedef {import("./Action").ActionConfig} ActionConfig */

class SSHCmd {
  /**
   * @return {Promise<ActionConfig>}
   */
  static async getConfig() { return {}; } // no config

  /** @return {boolean} */
  static useWS() { return true; }
}

export default SSHCmd;
