// @ts-check

import { butils } from '@cern/base-vue';

/**
 * @typedef {import("./Action").ActionConfig} ActionConfig
 * @typedef {import("./Action").ExtendedActionInfo} ExtendedActionInfo
 * @typedef {AppServer.Dimon.IPMIConsoleAction} IPMIConsoleAction
 */

class IPMIConsole {

  /**
   * @param {ExtendedActionInfo} info
   * @return {Promise<ActionConfig>}
   */
  static async getConfig(info) {
    if (!(/** @type {IPMIConsoleAction} */(info).host)) {
      return { output: { type: 'error', data: 'Host missing' } };
    }

    let query = '';
    if (info.menu && info.name) {
      query = `?menu=${encodeURIComponent(info.menu)}&` +
        `name=${encodeURIComponent(info.name)}`;
    }
    else if (info.zone && info.server && info.name) {
      query = `?zone=${encodeURIComponent(info.zone)}&` +
        `server=${encodeURIComponent(info.server)}&` +
        `name=${encodeURIComponent(info.name)}`;
    }
    else {
      return { output: { type: 'error', data: 'Action source missing' } };
    }

    return {
      extLink: {
        descr: 'Remote Console',
        url: `${butils.currentUrl()}/action/ipmi-console/` +
        /** @type {IPMIConsoleAction} */(info).host + query
      }
    };
  }

  /** @return {boolean} */
  static useWS() { return false; }
}

export default IPMIConsole;
