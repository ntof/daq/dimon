// @ts-check

/** @typedef {import("./Action").ActionConfig} ActionConfig */

class PLC {
  /**
   * @return {Promise<ActionConfig>}
   */
  static async getConfig() { return {}; } // no config

  /** @return {boolean} */
  static useWS() { return true; }
}

export default PLC;
