// @ts-check

import axios from 'axios';
import { butils } from '@cern/base-vue';
import { get } from 'lodash';

/**
 * @typedef {import("./Action").ActionConfig} ActionConfig
 */

class Ping {

  /**
   * @param {AppServer.Dimon.PingAction} info
   * @return {Promise<ActionConfig>}
   */
  static async getConfig(info) {
    if (!info.host) {
      return { output: { type: 'error', data: 'Missing host' } };
    }
    return (await this.isReachable(info.host)) ?
      { output: { type: 'stdout', data: `'${info.host}' reachable` } } :
      { output: { type: 'stderr', data: `'${info.host}' unreachable` } };
  }

  /**
   * @param {string} host
   * @return {Promise<boolean>}
   */
  static async isReachable(host) {
    try {
      const res = await axios.get(`${butils.currentUrl()}/ping`,
        {
          headers: { 'Accept': 'application/json' },
          params: { host }
        });
      return get(res, [ 'data', 'reachable' ], false);
    }
    catch (err) {
      throw new Error(`Cannot ping '${host}': ` +
        `${get(err, [ 'response', 'data' ]) || err.message}`);
    }
  }

  /** @return {boolean} */
  static useWS() { return false; }
}

export default Ping;
export const isReachable = Ping.isReachable;
