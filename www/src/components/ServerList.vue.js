import { find, get } from 'lodash';
// @ts-check

import Vue from 'vue';
import { mapState } from 'vuex';
import ServerStatus from './ServerStatus.vue';

/**
 * @typedef {V.Instance<typeof component> & { $butils: BaseVue.butils }} Instance
 */

const component = Vue.extend({
  name: 'ServerList',
  components: { ServerStatus },
  computed: {
    .../** @type {{ zone(): string|null, connected(): boolean }} */(
      mapState([ 'zone', 'connected' ])),
    /**
     * @this {Instance}
     * @return {boolean}
     */
    loading() {
      return this.connected &&
        this.$butils.isEmpty(this.$store.state.monitoring.zones);
    },
    /**
     * @this {Instance}
     * @return {Array<AppServer.Dimon.Server>}
     */
    servers() {
      /** @type {AppServer.Dimon.Zone} */
      const zone = find(get(this.$store.state, [ 'monitoring', 'zones' ]),
        [ 'name', this.zone ]) || {};
      return get(zone, 'servers', []);
    }
  }
});
export default component;
