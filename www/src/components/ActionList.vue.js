// @ts-check

import Vue from 'vue';

const component = Vue.extend({
  name: 'ActionList',
  props: {
    title: { type: String, default: '' },
    actions: { type: Array, default: null }
  }
});
export default component;
