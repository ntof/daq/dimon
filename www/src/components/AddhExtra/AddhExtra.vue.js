/* eslint-disable max-lines */
/* eslint-disable vue/one-component-per-file */
// @ts-check

import Vue from "vue";
import { assign, cloneDeep, filter, forEach, get, isArray, isEmpty, isNil, values } from 'lodash';
import {
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { ParamBuilderMixin, UrlUtilitiesMixin, errorPrefix } from '../../utilities';
import { DicXmlCmd, DicXmlValue, xml } from "@ntof/redim-client";
import { mapState } from "vuex";

const wrapError = errorPrefix.bind(null, 'Addh: ');

/** @enum {number} */
const DataSourceType = {
  UNKNOWN: -1,
  SINGLE_DATA: 0,
  ALL_DATA: 1,
  VALUE: 2
};

class ExtraDataSource {
  /**
     * @param {number} index
     */
  constructor(index) {
    this.id = index;
    this.type = DataSourceType.UNKNOWN;
    this.fromService = "";
    this.fromName = "";
    this.destination = "";
    this.destinationPrefix = "";
  }

  toXmlJS() {
    if (this.type === DataSourceType.SINGLE_DATA) {
      return { '$': {
        fromService: this.fromService, fromName: this.fromName, destination: this.destination
      } };
    }
    if (this.type === DataSourceType.ALL_DATA) {
      return { '$': {
        fromService: this.fromService, destinationPrefix: this.destinationPrefix
      } };
    }
    if (this.type === DataSourceType.VALUE) {
      return { '$': {
        fromService: this.fromService, destination: this.destination
      } };
    }
  }
}

/**
 * @typedef {import('redim-client').XmlJsObject } XmlJsObject
 *
 * @typedef {V.Instance<typeof component> &
 *   V.Instance<typeof UrlUtilitiesMixin> &
 *   V.Instance<typeof ParamBuilderMixin> &
 *   V.Instance<ReturnType<KeyboardEventMixin>>} Instance
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'AddhExtra',
  mixins: [
    KeyboardEventMixin({ local: true, checkOnInputs: true }),
    UrlUtilitiesMixin,
    ParamBuilderMixin
  ],
  props: {
    zone: { type: String, default: '' },
    dns: { type: String, default: '' }
  },
  /**
   * @return {{
   *  client?: DicXmlValue | null,
   *  loading: boolean,
   *  inEdit: boolean
   *  originalData: Record<number, ExtraDataSource>,
   *  editData: Record<number, ExtraDataSource>,
   *  index: number,
   *  DataSourceType: typeof DataSourceType,
   *  isSendingCommand: boolean
   * }}
   */
  data() {
    return {
      client: null, loading: false, inEdit: false,
      originalData: {}, editData: {}, index: 0,
      DataSourceType: DataSourceType, isSendingCommand: false
    };
  },
  computed: {
    ...mapState('ui', [ 'showKeyHints' ]),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isConnected() {
      return !this.loading;
    },
    /**
     * @this {Instance}
     * @returns {Array<ExtraDataSource>}
     */
    singleEditData() {
      return filter(values(this.editData), (d) => d.type === DataSourceType.SINGLE_DATA);
    },
    /**
     * @this {Instance}
     * @returns {Array<ExtraDataSource>}
     */
    allEditData() {
      return filter(values(this.editData), (d) => d.type === DataSourceType.ALL_DATA);
    },
    /**
     * @this {Instance}
     * @returns {Array<ExtraDataSource>}
     */
    valueEditData() {
      return filter(values(this.editData), (d) => d.type === DataSourceType.VALUE);
    }
  },
  watch: {
    /** @this {Instance} */
    dns() { this.monitor(); }
  },
  /** @this {Instance} */
  mounted() {
    this.onKey('e', () => this.setEdit(true));
    this.onKey('esc', () => this.setEdit(false));
    this.onKey('ctrl-s-keydown', this.submit);
    this.monitor();
  },
  /** @this {Instance} */
  beforeDestroy() {
    this.close();
  },
  methods: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value/*: any */) {
      this.inEdit = value;
      // Reset editData with original data if exit from edit mode
      if (!value) { this.editData = cloneDeep(this.originalData); }
    },
    /** @this {Instance} */
    close() {
      if (this.client) {
        this.client.removeListener('value', this.handleValue);
        this.client.removeListener('error', this.handleError);
        this.client.close();
      }
      this.client = null;
    },
    /** @this {Instance} */
    async monitor() {
      this.close();
      // @ts-ignore: data is set
      assign(this, this.$options.data());
      this.loading = true;

      this.client = new DicXmlValue('ADDH/Extra',
        { proxy: this.currentUrl() }, this.dnsUrl(this.dns));
      this.client.on('error', this.handleError);
      this.client.on('value', this.handleValue);
      this.client.promise().catch(this.handleError);
    },
    /**
     * @this {Instance}
     * @param {any} value
     */
    handleValue(value /*: any */) {
      if (!value) { return; }

      // Force inEdit false
      this.setEdit(false);
      // Clear data
      this.originalData = {};
      this.editData = {};
      this.index = 0; // Code used for refs

      try {
        const dataJs = xml.toJs(value);

        // Process data array ( could be SINGLE_DATA or ALL_DATA item)
        let dataArray = get(dataJs, 'data');
        if (!isArray(dataArray)) { dataArray = [ dataArray ]; }
        forEach(dataArray, (data) => {
          const attrObj = get(data, '$');
          if (isNil(attrObj)) { return; }
          const newExtra = new ExtraDataSource(this.index++);
          assign(newExtra, attrObj);
          if (isEmpty(newExtra.fromService)) { return; }

          if (!isEmpty(newExtra.fromName) && !isEmpty(newExtra.destination)) {
            newExtra.type = DataSourceType.SINGLE_DATA;
          }
          else if (!isEmpty(newExtra.destinationPrefix)) {
            newExtra.type = DataSourceType.ALL_DATA;
          }
          Vue.set(this.originalData, newExtra.id, newExtra);
        });

        // Process value array (could be valueData only)
        let valueArray = get(dataJs, 'value');
        if (!isArray(valueArray)) { valueArray = [ valueArray ]; }
        forEach(valueArray, (value) => {
          const attrObj = get(value, '$');
          if (isNil(attrObj)) { return; }
          const newExtra = new ExtraDataSource(this.index++);
          assign(newExtra, attrObj);
          newExtra.type = DataSourceType.VALUE;
          Vue.set(this.originalData, newExtra.id, newExtra);
        });

        this.editData = cloneDeep(this.originalData);
      }
      catch (err) { logger.error(err); }
      finally {
        this.loading = false;
      }
    },
    /**
     * @this {Instance}
     * @param {any} err
     */
    handleError(err) {
      logger.error(err);
    },
    /**
     * @this {Instance}
     * @param {Array<ExtraDataSource>} array
     * @param {number} id
     */
    removeSource(id) {
      Vue.delete(this.editData, id);
    },
    /**
     * @this {Instance}
     * @param {DataSourceType} type
     */
    addSource(type) {
      const newExtra = new ExtraDataSource(this.index++);
      newExtra.type = type;
      Vue.set(this.editData, newExtra.id, newExtra);
    },
    /**
     * @this {Instance}
     * @param {ExtraDataSource} data
     */
    hasMissingFields(data) {
      if (data.type === DataSourceType.UNKNOWN || isEmpty(data.fromService)) {
        return true;
      }
      switch (data.type) {
      case DataSourceType.SINGLE_DATA:
        return isEmpty(data.fromName) || isEmpty(data.destination);
      case DataSourceType.ALL_DATA:
        return isEmpty(data.destinationPrefix);
      case DataSourceType.VALUE:
        return isEmpty(data.destination);
      }
    },
    /** @this {Instance} */
    keepFocus() {
      this.$nextTick(() => {
        /** @type HTMLElement */(this.$el).focus();
      });
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent | null} event
     */
    async submit(event /*: KeyboardEvent */) {
      if (event) {
        event.preventDefault(); /* prevent ctrl-s to open dialog */
        if (!this.inEdit) { return; } // Keyboard Shortcut Guard
      }
      // Build the extraObj
      /** @type {XmlJsObject} */
      const extraObj = { data: [], value: [] };
      // Iterate over editData
      forEach(this.editData, (data) => {
        if (this.hasMissingFields(data)) { return; }
        if (data.type === DataSourceType.VALUE) {
          extraObj.value.push(data.toXmlJS());
        }
        else {
          extraObj.data.push(data.toXmlJS());
        }
      });
      this.changeExtra(extraObj);
    },
    /**
     * @this {Instance}
     * @param {any} extraObj
     */
    changeExtra(extraObj) {
      this.wrapCmd(this.extraCmd, extraObj);
    },
    /**
     * @this {Instance}
     * @param {any} extraObj
     */
    extraCmd(extraObj) {
      return DicXmlCmd.invoke('ADDH/Command',
        { command: { '$': { name: 'extra' }, extra: extraObj } },
        { proxy: this.currentUrl() }, this.dnsUrl(this.dns))
      .catch(wrapError);
    },
    /**
     * @this {Instance}
     * @param {function(any): Promise<void>} cmd
     * @param {any} args
     */
    wrapCmd(cmd, ...args) {
      this.isSendingCommand = true;
      // @ts-ignore
      cmd.call(this, ...args)
      .catch((/** @type any */err) => logger.error(err))
      .finally(() => {
        this.isSendingCommand = false;
        this.setEdit(false);
        this.keepFocus();
      });
    }
  }
});
export default component;
