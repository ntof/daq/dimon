// @ts-check

import Vue from "vue";
import { mapState } from 'vuex';

import AddhExtra from './AddhExtra.vue';

/**
  * @typedef {V.Instance<typeof component>} Instance
  */

const component = Vue.extend({
  name: 'AddhExtraLayout',
  components: { AddhExtra },
  computed: {
    ...mapState('monitoring', [ 'zones' ])
  }
});
export default component;
