// @ts-check

import { TITLE, VERSION } from './Consts';
import Vue from 'vue';
import { mapState } from 'vuex';
import { filter, noop, omit } from 'lodash';
import ActionList from './components/ActionList.vue';
import ActionDialog from './components/Actions/ActionDialog.vue';
import { sources } from './store';
import ActionMixin from './mixins/ActionMixin';

/**
 * @typedef {V.Instance<typeof component> & { $butils: BaseVue.butils } &
 *  V.Instance<typeof ActionMixin> } Instance
 */

const options = {
  /** @type {NodeJS.Timeout|null} */
  _timer: null
};

const component = /** @type {V.Constructor<typeof options, any>} */(Vue).extend({
  name: 'App',
  components: { ActionList, ActionDialog },
  mixins: [ ActionMixin ],
  ...options,
  /** @return {{ TITLE: string, VERSION: string, blink: boolean }} */
  data() { return { TITLE, VERSION, blink: false }; },
  computed: {
    ...mapState({ selectedZone: 'zone', connected: 'connected' }),
    ...mapState('monitoring', [ 'menus', 'zones' ]),
    /**
     * @this {Instance}
     * @returns {boolean}
     */
    isExtraDataSelected() {
      return this.$route.path === '/addh-extra';
    }
  },
  watch: {
    zones() {
      if (this.$options._timer !== null) {
        clearTimeout(this.$options._timer);
        this.$options._timer = null;
      }
      this.blink = true;
      this.$options._timer = setTimeout(() => { this.blink = false; }, 1000);
    }
  },
  /**
   * @this {Instance}
   */
  mounted() {
    sources.monitoring.connect(this.$butils.currentUrl() + '/monitoring');
  },
  destroyed() {
    sources.monitoring.close();
  },
  methods: {
    /**
     * @param {string} zone
     */
    selectZone(zone) {
      if (zone) {
        const query = omit(this.$route.query, [ 'zone' ]);
        query.zone = zone;
        this.$router.push({ path: '/', query }).catch(noop);
      }
    },
    /**
     * @param {string} zone
     */
    goToAddhExtra() {
      const query = omit(this.$route.query, [ 'zone' ]); // Remove zone
      this.$router.push({ path: '/addh-extra', query }).catch(noop);
    },
    /**
     * @this {Instance}
     * @param {Array<AppServer.Dimon.Action>} actions
     * @return {Array<AppServer.Dimon.Action>}
     */
    getAllowedActions(actions) {
      return filter(actions, (act) => this.isAllowed(act.allowed));
    }
  }
});

export default component;
